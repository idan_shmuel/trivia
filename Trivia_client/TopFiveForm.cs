﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class TopFiveForm : Form
    {
        private Socket socket;
        private General general;
        private string username;
        public TopFiveForm(Socket socket, string username)
        {
            InitializeComponent();
            this.general = new General();
            this.socket = socket;
            this.username = username;

            //user statistics code
            string messageCode = "00001000";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 1:
                        this.topFive.Text = "";
                        for (int i = 0; i < recvJson["highScores"].Count(); i++)
                        {
                            this.topFive.Text += recvJson["highScores"][i].ToString().Trim('[', ']').Replace(",", "").Replace(" ", "").Substring(3).Replace('"', ':');
                        }
                        break;
                }
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
                Application.Exit();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            StatisticsForm statisticsForm = new StatisticsForm(this.socket, this.username);
            this.Hide();
            statisticsForm.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
