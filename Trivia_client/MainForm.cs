﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class MainForm : Form
    {
        private Socket socket;
        public MainForm()
        {
            DialogResult result = DialogResult.OK;
            InitializeComponent();
            while (result == DialogResult.OK)
            {
                try
                {
                    this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    IPAddress ipAdd = System.Net.IPAddress.Parse("127.0.0.1");
                    IPEndPoint remoteEP = new IPEndPoint(ipAdd, 5353);
                    this.socket.Connect(remoteEP);
                    result = DialogResult.Cancel;
                }
                catch
                {
                    MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                    result = MessageBox.Show("You do not have a server running\n\nOk - try again.\nCancel - close client.", "Connection Problem", buttons, MessageBoxIcon.Error);
                    if (result == DialogResult.Cancel)
                    {
                        this.Close();
                    }
                }
            }
        }

        private void signInButton_Click(object sender, EventArgs e)
        {
            SignInForm signInForm = new SignInForm(this.socket);
            this.Hide();
            signInForm.ShowDialog();
        }

        private void signUpButton_Click(object sender, EventArgs e)
        {
            SignUpForm signUpForm = new SignUpForm(this.socket);
            this.Hide();
            signUpForm.ShowDialog();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
           Application.Exit();
        }

    }
}
