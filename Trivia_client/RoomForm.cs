﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class RoomForm : Form
    {
        private bool isAdmin;
        private Socket socket;
        private RoomData roomDataStruct;
        private General general;
        private string username;
        public RoomForm(bool isAdmin, Socket socket, RoomData roomDataStruct, string username)
        {
            InitializeComponent();
            this.isAdmin = isAdmin;
            this.username = username;
            this.general = new General();
            if(this.isAdmin)
            {
                this.welcomeLabel.Text = "You are the admin of '" + roomDataStruct.roomName + "'";
                this.leaveRoomButton.Text = "Close room";
            }
            else
            {
                this.welcomeLabel.Text = "You are a member in '" + roomDataStruct.roomName + "'";
                this.leaveRoomButton.Text = "Leave room";
                this.startGameButton.Enabled = false;
            }
            this.socket = socket;
            this.roomDataStruct = roomDataStruct;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            leaveRoomButton_Click(sender, e);
        }

        private void leaveRoomButton_Click(object sender, EventArgs e)
        {
            if(this.isAdmin)
            {
                //close room code
                string messageCode = "00001001";
                string json = "";
                JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
                if (recvJson["status"] != null)
                {
                    int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                    switch (statusCode)
                    {
                        case 1:
                            this.Close();
                            CreateRoomForm createRoomForm = new CreateRoomForm(this.socket, this.username);
                            this.Hide();
                            createRoomForm.ShowDialog();
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Server ERROR.");
                }
            }
            else
            {
                //leave room code
                string messageCode = "00001100";
                string json = "";
                JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
                if (recvJson["status"] != null)
                {
                    int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                    switch (statusCode)
                    {
                    case 1:
                        this.Close();
                        JoinRoomForm joinRoomForm = new JoinRoomForm(this.socket, this.username);
                        this.Hide();
                        joinRoomForm.ShowDialog();
                        break;
                    }
                }
                else
                {
                    MessageBox.Show("Server ERROR.");
                }
            }
        }

        private void startGameButton_Click(object sender, EventArgs e)
        {
            //start game code
            string messageCode = "00001010";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                case 1:
                    gameForm GameForm = new gameForm(this.socket, this.roomDataStruct, this.isAdmin, this.username);
                    this.Hide();
                    GameForm.ShowDialog();
                    break;
                }
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(!this.isAdmin)
            {
                //get room state code
                string messageCode = "00001011";
                string json = "";
                JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
                if (recvJson["status"] != null)
                {
                    int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                    switch (statusCode)
                    {
                    case 1:
                        if (recvJson["hasGameBegun"].ToString() == "True")
                        {
                            timer1.Stop();
                            gameForm GameForm = new gameForm(this.socket, this.roomDataStruct, false, this.username);
                            this.Hide();
                            GameForm.ShowDialog();
                        }
                        break;
                    }
                }
                else
                {
                    MessageBox.Show("Server Error");
                }
            }

        }
    }
}
