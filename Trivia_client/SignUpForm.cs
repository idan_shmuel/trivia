﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class SignUpForm : Form
    {
        private bool userFirst, passwordFirst, EmailFirst, addressFirst, phoneNumberFirst, birthDateFirst, passwordChar;
        private Socket socket;
        private General general;
        public SignUpForm(Socket socket)
        {
            InitializeComponent();
            this.socket = socket;
            this.userFirst = true;
            this.passwordFirst = true;
            this.EmailFirst = true;
            this.addressFirst = true;
            this.phoneNumberFirst = true;
            this.birthDateFirst = true;
            this.passwordChar = false;
            this.general = new General();
        }

        private void usernameBox_GotFocus(object sender, EventArgs e)
        {
            if (this.userFirst)
            {
                this.usernameBox.Clear();
                this.userFirst = false;
            }
        }

        private void passwordBox_GotFocus(object sender, EventArgs e)
        {
            if (this.passwordFirst)
            {
                this.passwordBox.Clear();
                this.passwordBox.PasswordChar = '*';
                this.passwordChar = true;
                this.passwordFirst = false;
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            errorBox.Clear();
            SignupRequest signupRequestStruct = new SignupRequest(usernameBox.Text, passwordBox.Text, EmailBox.Text, addressBox.Text, phoneNumberBox.Text, birthDateBox.Text);
            //signup code
            string messageCode = "00000010";
            string json = JsonConvert.SerializeObject(signupRequestStruct);
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 1:
                        this.Close();
                        MainForm mainForm = new MainForm();
                        this.Hide();
                        mainForm.ShowDialog();
                        break;
                    case 2:
                        errorBox.Text = "Error! User is already exist.";
                        break;
                    case 3:
                        errorBox.Text = "Error! Password must include - [0-9], [A-z], [!@#$%^&*].";
                        break;
                    case 4:
                        errorBox.Text = "Error! Email has to be [A-z][0-9]@(domain).";
                        break;
                    case 5:
                        errorBox.Text = "Error! Address has to be [A-z], [0-9], [A-z].";
                        break;
                    case 6:
                        errorBox.Text = "Error! Phone number has to be prefix-number.";
                        break;
                    case 7:
                        errorBox.Text = "Error! Birth date has to be DD.MM.YYYY or DD/MM/YYYY.";
                        break;
                    case 8:
                        errorBox.Text = "Error! Username has to be at least 1 character.";
                        break;
                }
            }
            else
            {
                MessageBox.Show("Server Error");
            }

        }

        private void EmailBox_GotFocus(object sender, EventArgs e)
        {
            if (this.EmailFirst)
            {
                this.EmailBox.Clear();
                this.EmailFirst = false;
            }
        }

        private void birthDateBox_GotFocus(object sender, EventArgs e)
        {
            if (this.birthDateFirst)
            {
                this.birthDateBox.Clear();
                this.birthDateFirst = false;
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void showPasswordButton_Click(object sender, EventArgs e)
        {
            if (this.passwordChar)
            {
                this.passwordBox.PasswordChar = '\0';
                this.passwordChar = false;
                this.showPasswordButton.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                this.passwordBox.PasswordChar = '*';
                this.passwordChar = true;
                this.showPasswordButton.BackColor = System.Drawing.Color.Red;
            }

        }

        private void addressBox_GotFocus(object sender, EventArgs e)
        {
            if (this.addressFirst)
            {
                this.addressBox.Clear();
                this.addressFirst = false;
            }
        }

        private void phoneNumberBox_GotFocus(object sender, EventArgs e)
        {
            if (this.phoneNumberFirst)
            {
                this.phoneNumberBox.Clear();
                this.phoneNumberFirst = false;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            MainForm mainForm = new MainForm();
            this.Hide();
            mainForm.ShowDialog();
        }
    }
}
