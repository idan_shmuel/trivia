﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class MyStatisticsForm : Form
    {
        private Socket socket;
        private General general;
        private string username;
        public MyStatisticsForm(Socket socket, string username)
        {
            InitializeComponent();
            this.socket = socket;
            this.general = new General();
            this.username = username;
            //user statistics code
            string messageCode = "00010001";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 1:
                        this.userNumOfCorrectAnswers.Text = recvJson["correctAnswerCount"].ToString();
                        this.userNumOfPlayerGames.Text = recvJson["numOfPlayerGames"].ToString();
                        this.userNumOfTotalAnswers.Text = recvJson["totalAnswerCount"].ToString(); String.Format("{0:0.00}", 123.4567);
                        this.userAverageAnswerTime.Text = String.Format("{0:0.00}", recvJson["averageAnswerTime"]) + " seconds";
                        break;
                }
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            StatisticsForm statisticsForm = new StatisticsForm(this.socket, this.username);
            this.Hide();
            statisticsForm.ShowDialog();
        }
        private void MyStatisticsForm_Load(object sender, EventArgs e)
        {

        }
    }
}

