﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class gameForm : Form
    {
        private Socket socket;
        private string username;
        private RoomData roomDataStruct;
        private int currentTime;
        private int currentQuestionNum;
        private int currentCorrectQuestionNum;
        private General general;
        private bool isAdmin;
        public gameForm(Socket socket, RoomData roomDataStruct, bool isAdmin, string username)
        {
            InitializeComponent();
            this.socket = socket;
            this.roomDataStruct = roomDataStruct;
            this.username = username;
            this.isAdmin = isAdmin;
            this.currentQuestionNum = 0;
            this.currentCorrectQuestionNum = 0;
            this.timePerQuestion.Text = this.roomDataStruct.answerTimeout.ToString();
            this.currentTime = this.roomDataStruct.answerTimeout;
            this.general = new General();
            getNextQuestion();
            this.correctAnswers.Text = "Score: " + this.currentCorrectQuestionNum.ToString() + "/" + this.currentQuestionNum.ToString();
            this.currentQuestionNum++;
            this.questionCount.Text = "Question: " + this.currentQuestionNum.ToString() + "/" + this.roomDataStruct.questionCount.ToString();
        }

        private void send_Click(object sender, EventArgs e)
        {
            submitAnswer();
            if (this.currentQuestionNum == this.roomDataStruct.questionCount)
            {
                //get results form || wait until all the users finish.
                this.timer1.Stop();
                MessageBox.Show("finish");
            }
            else
            {
                getNextQuestion();
                this.currentQuestionNum++;
                this.questionCount.Text = "Question: " + this.currentQuestionNum.ToString() + "/" + this.roomDataStruct.questionCount.ToString();
                this.timePerQuestion.Text = this.roomDataStruct.answerTimeout.ToString();
                this.currentTime = this.roomDataStruct.answerTimeout;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.currentTime--;
            this.timePerQuestion.Text = this.currentTime.ToString();
            if (this.currentTime == 0)
            {
                if (this.currentQuestionNum == this.roomDataStruct.questionCount)
                {
                    //get results form || wait until all the users finish.
                    this.timer1.Stop();
                    MessageBox.Show("finish");
                }
                else
                {
                    getNextQuestion();
                    this.currentQuestionNum++;
                    this.questionCount.Text = "Question: " + this.currentQuestionNum.ToString() + "/" + this.roomDataStruct.questionCount.ToString();
                    this.correctAnswers.Text = "Score: " + this.currentCorrectQuestionNum.ToString() + "/" + this.currentQuestionNum.ToString();
                    this.timePerQuestion.Text = this.roomDataStruct.answerTimeout.ToString();
                    this.currentTime = this.roomDataStruct.answerTimeout;
                }
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
           //leave game code
            string messageCode = "00010000";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 1:
                        Application.Exit();
                        break;
                }
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }
        private void getNextQuestion()
        {
            this.answer1.Checked = false;
            this.answer2.Checked = false;
            this.answer3.Checked = false;
            this.answer4.Checked = false;
            //get question code
            string messageCode = "00001111";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 1:
                        this.Question.Text = recvJson["question"].ToString();
                        this.answer1.Text = recvJson["answers"][0][1].ToString();
                        this.answer2.Text = recvJson["answers"][1][1].ToString();
                        this.answer3.Text = recvJson["answers"][2][1].ToString();
                        this.answer4.Text = recvJson["answers"][3][1].ToString();
                        this.currentTime = int.Parse(this.timePerQuestion.Text);
                        break;
                }
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }

        private void submitAnswer()
        {
            //submit answer code
            string messageCode = "00001110";
            // "" symbolize that the user doesn't choce anything answer.
            SubmitAnswerRequest submitAnswerRequest = new SubmitAnswerRequest("");
            
            if (this.answer1.Checked)
            {
                submitAnswerRequest.answerSelected = this.answer1.Text;
            }
            else if (this.answer2.Checked)
            {
                submitAnswerRequest.answerSelected = this.answer2.Text;
            }
            else if (this.answer3.Checked)
            {
                submitAnswerRequest.answerSelected = this.answer3.Text;
            }
            else if (this.answer4.Checked)
            {
                submitAnswerRequest.answerSelected = this.answer4.Text;
            }

            // when the user chose answer.
            if(submitAnswerRequest.answerSelected != "")
            {
                string json = JsonConvert.SerializeObject(submitAnswerRequest);
                JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
                if (recvJson["status"] != null)
                {
                    int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                    switch (statusCode)
                    {
                        case 0:
                            this.correctAnswers.Text = "Score: " + this.currentCorrectQuestionNum.ToString() + "/" + this.currentQuestionNum.ToString();
                            break;
                        case 1:
                            this.currentCorrectQuestionNum++;
                            this.correctAnswers.Text = "Score: " + this.currentCorrectQuestionNum.ToString() + "/" + this.currentQuestionNum.ToString();
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Server ERROR.");
                }
            }
            else
            {
                this.correctAnswers.Text = "Score: " + this.currentCorrectQuestionNum.ToString() + "/" + this.currentQuestionNum.ToString();
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.timer1.Stop();
            //leave game code
            string messageCode = "00010000";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 1:
                        this.Close();
                        MenuForm menuForm = new MenuForm(this.username, this.socket);
                        this.Hide();
                        menuForm.ShowDialog();
                        break;
                }
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        
        }
    }


}
