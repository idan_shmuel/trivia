﻿namespace Trivia_client
{
    partial class MyStatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.playerAverageAnswerTime = new System.Windows.Forms.Label();
            this.numOfCorrectAnswers = new System.Windows.Forms.Label();
            this.numOfPlayerGames = new System.Windows.Forms.Label();
            this.numOfTotalAnswers = new System.Windows.Forms.Label();
            this.userAverageAnswerTime = new System.Windows.Forms.Label();
            this.userNumOfCorrectAnswers = new System.Windows.Forms.Label();
            this.userNumOfPlayerGames = new System.Windows.Forms.Label();
            this.userNumOfTotalAnswers = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.backButton.Location = new System.Drawing.Point(713, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 35);
            this.backButton.TabIndex = 11;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.SystemColors.MenuBar;
            this.exitButton.Font = new System.Drawing.Font("Snap ITC", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(12, 12);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 35);
            this.exitButton.TabIndex = 10;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.usernameLabel.Location = new System.Drawing.Point(321, 63);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(134, 31);
            this.usernameLabel.TabIndex = 12;
            this.usernameLabel.Text = "username";
            // 
            // playerAverageAnswerTime
            // 
            this.playerAverageAnswerTime.AutoSize = true;
            this.playerAverageAnswerTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.playerAverageAnswerTime.Location = new System.Drawing.Point(151, 130);
            this.playerAverageAnswerTime.Name = "playerAverageAnswerTime";
            this.playerAverageAnswerTime.Size = new System.Drawing.Size(198, 24);
            this.playerAverageAnswerTime.TabIndex = 13;
            this.playerAverageAnswerTime.Text = "Average Answer Time";
            // 
            // numOfCorrectAnswers
            // 
            this.numOfCorrectAnswers.AutoSize = true;
            this.numOfCorrectAnswers.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.numOfCorrectAnswers.Location = new System.Drawing.Point(151, 180);
            this.numOfCorrectAnswers.Name = "numOfCorrectAnswers";
            this.numOfCorrectAnswers.Size = new System.Drawing.Size(149, 24);
            this.numOfCorrectAnswers.TabIndex = 14;
            this.numOfCorrectAnswers.Text = "Correct Answers";
            // 
            // numOfPlayerGames
            // 
            this.numOfPlayerGames.AutoSize = true;
            this.numOfPlayerGames.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.numOfPlayerGames.Location = new System.Drawing.Point(151, 230);
            this.numOfPlayerGames.Name = "numOfPlayerGames";
            this.numOfPlayerGames.Size = new System.Drawing.Size(194, 24);
            this.numOfPlayerGames.TabIndex = 15;
            this.numOfPlayerGames.Text = "num Of Player Games";
            // 
            // numOfTotalAnswers
            // 
            this.numOfTotalAnswers.AutoSize = true;
            this.numOfTotalAnswers.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.numOfTotalAnswers.Location = new System.Drawing.Point(151, 280);
            this.numOfTotalAnswers.Name = "numOfTotalAnswers";
            this.numOfTotalAnswers.Size = new System.Drawing.Size(129, 24);
            this.numOfTotalAnswers.TabIndex = 16;
            this.numOfTotalAnswers.Text = "Total Answers";
            // 
            // userAverageAnswerTime
            // 
            this.userAverageAnswerTime.AutoSize = true;
            this.userAverageAnswerTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userAverageAnswerTime.Location = new System.Drawing.Point(446, 130);
            this.userAverageAnswerTime.Name = "userAverageAnswerTime";
            this.userAverageAnswerTime.Size = new System.Drawing.Size(186, 20);
            this.userAverageAnswerTime.TabIndex = 17;
            this.userAverageAnswerTime.Text = "userAverageAnswerTime";
            // 
            // userNumOfCorrectAnswers
            // 
            this.userNumOfCorrectAnswers.AutoSize = true;
            this.userNumOfCorrectAnswers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userNumOfCorrectAnswers.Location = new System.Drawing.Point(446, 180);
            this.userNumOfCorrectAnswers.Name = "userNumOfCorrectAnswers";
            this.userNumOfCorrectAnswers.Size = new System.Drawing.Size(203, 20);
            this.userNumOfCorrectAnswers.TabIndex = 18;
            this.userNumOfCorrectAnswers.Text = "userNumOfCorrectAnswers";
            // 
            // userNumOfPlayerGames
            // 
            this.userNumOfPlayerGames.AutoSize = true;
            this.userNumOfPlayerGames.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userNumOfPlayerGames.Location = new System.Drawing.Point(446, 230);
            this.userNumOfPlayerGames.Name = "userNumOfPlayerGames";
            this.userNumOfPlayerGames.Size = new System.Drawing.Size(185, 20);
            this.userNumOfPlayerGames.TabIndex = 19;
            this.userNumOfPlayerGames.Text = "userNumOfPlayerGames";
            // 
            // userNumOfTotalAnswers
            // 
            this.userNumOfTotalAnswers.AutoSize = true;
            this.userNumOfTotalAnswers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userNumOfTotalAnswers.Location = new System.Drawing.Point(446, 280);
            this.userNumOfTotalAnswers.Name = "userNumOfTotalAnswers";
            this.userNumOfTotalAnswers.Size = new System.Drawing.Size(186, 20);
            this.userNumOfTotalAnswers.TabIndex = 20;
            this.userNumOfTotalAnswers.Text = "userNumOfTotalAnswers";
            // 
            // MyStatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Trivia_client.Properties.Resources.signUpScreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.userNumOfTotalAnswers);
            this.Controls.Add(this.userNumOfPlayerGames);
            this.Controls.Add(this.userNumOfCorrectAnswers);
            this.Controls.Add(this.userAverageAnswerTime);
            this.Controls.Add(this.numOfTotalAnswers);
            this.Controls.Add(this.numOfPlayerGames);
            this.Controls.Add(this.numOfCorrectAnswers);
            this.Controls.Add(this.playerAverageAnswerTime);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.exitButton);
            this.MaximizeBox = false;
            this.Name = "MyStatisticsForm";
            this.Text = "MyStatisticsForm";
            this.Load += new System.EventHandler(this.MyStatisticsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label playerAverageAnswerTime;
        private System.Windows.Forms.Label numOfCorrectAnswers;
        private System.Windows.Forms.Label numOfPlayerGames;
        private System.Windows.Forms.Label numOfTotalAnswers;
        private System.Windows.Forms.Label userAverageAnswerTime;
        private System.Windows.Forms.Label userNumOfCorrectAnswers;
        private System.Windows.Forms.Label userNumOfPlayerGames;
        private System.Windows.Forms.Label userNumOfTotalAnswers;
    }
}