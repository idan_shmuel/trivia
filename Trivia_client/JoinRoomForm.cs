﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;


namespace Trivia_client
{
    public partial class JoinRoomForm : Form
    {
        private Socket socket;
        private string username;
        private bool noRooms;
        private General general;
        private RoomData[] roomDataStruct;
        public JoinRoomForm(Socket socket, string username)
        {
            InitializeComponent();
            this.username = username;
            this.socket = socket;
            noRooms = true;
            this.general = new General();
        }
        private void backButton_Click(object sender, EventArgs e)
        {
            this.timer1.Stop();
            this.Close();
            MenuForm menuForm = new MenuForm(this.username, this.socket);
            this.Hide();
            menuForm.ShowDialog();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.timer1.Stop();
            Application.Exit();
        }

        private void JoinButton_Click(object sender, EventArgs e)
        {
            int selectedIndex = this.RoomsList.SelectedIndex;
            JoinRoomRequest joinRoomRequest = new JoinRoomRequest(selectedIndex);

            //join room code
            string messageCode = "00000110";
            string json = JsonConvert.SerializeObject(joinRoomRequest);
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 0:
                        MessageBox.Show("Room doesn't exist.");
                        break;
                    case 1:
                        this.timer1.Stop();
                        RoomForm room = new RoomForm(false, this.socket, this.roomDataStruct[selectedIndex], this.username);
                        this.Hide();
                        room.ShowDialog();
                        break;
                }
            }
            else
            {
                MessageBox.Show("Server Error");
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.RoomsList.Items.Clear();
            //get rooms code
            string messageCode = "00000100";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 0:
                        MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                        if(noRooms)
                        {
                            noRooms = false;
                            DialogResult result = MessageBox.Show("There are no available rooms", "Room Error", buttons, MessageBoxIcon.Error);
                        }
                        
                        break;
                    case 1:
                        noRooms = false;
                        this.roomDataStruct = new RoomData[recvJson["rooms"].Count()];
                        for (int i = 0; i < recvJson["rooms"].Count(); i++)
                        {
                            this.RoomsList.Items.Add(recvJson["rooms"][i]["name"]);
                            this.roomDataStruct[i].answerTimeout = int.Parse(recvJson["rooms"][i]["timePerQuestion"].ToString());
                            this.roomDataStruct[i].roomName = recvJson["rooms"][i]["name"].ToString();
                            this.roomDataStruct[i].questionCount = int.Parse(recvJson["rooms"][i]["questionCount"].ToString());
                            this.roomDataStruct[i].maxPlayers = int.Parse(recvJson["rooms"][i]["maxPlayers"].ToString());
                        }
                        break;
                }
            }
            else
            {
                MessageBox.Show("Server Error");
            }
        }

        private void RoomsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void undoButton_Click(object sender, EventArgs e)
        {
            this.RoomsList.ClearSelected();
            timer1.Start();
        }
    }
}
