﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class CreateRoomForm : Form
    {
        private bool nameFirst, maxFirst, numberFirst, timeFirst;
        private string username;
        private Socket socket;
        private General general;
        public CreateRoomForm(Socket socket, string username)
        {
            InitializeComponent();
            this.socket = socket;
            this.username = username;
            this.nameFirst = true;
            this.maxFirst = true;
            this.numberFirst = true;
            this.timeFirst = true;
            this.general = new General();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            //logout code
            string messageCode = "00000011";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            int statusCode = Convert.ToInt32(recvJson["status"].ToString());
            if (statusCode == 1)
            {
                Application.Exit();
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuForm menuForm = new MenuForm(this.username, this.socket);
            this.Hide();
            menuForm.ShowDialog();

        }

        private void roomNameBox_GotFocus(object sender, EventArgs e)
        {
            if(this.nameFirst)
            {
                this.roomNameBox.Clear();
                this.nameFirst = false;
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            if(!int.TryParse(this.maxPlayersBox.Text, out int a) || !int.TryParse(this.questionCountBox.Text, out int b) || !int.TryParse(this.answerTimeBox.Text, out int c) || int.TryParse(this.roomNameBox.Text, out int d)|| int.Parse(this.questionCountBox.Text) > 10 || int.Parse(this.questionCountBox.Text) < 1)
            {
                MessageBox.Show("Please enter only numbers in max players, number of questions and time per question.\nquestion count need to be between 1-10.");
            }
            else
            {
                RoomData roomDataStruct = new RoomData(this.roomNameBox.Text, int.Parse(this.maxPlayersBox.Text), int.Parse(this.questionCountBox.Text), int.Parse(this.answerTimeBox.Text));
               
                //create room code
                string messageCode = "00000111";
                string json = JsonConvert.SerializeObject(roomDataStruct);
                JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
                if (recvJson["status"] != null)
                {
                    int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                    switch (statusCode)
                    {
                        case 1:
                            RoomForm room = new RoomForm(true, this.socket, roomDataStruct, this.username);
                            this.Hide();
                            room.ShowDialog();
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Srever Error");
                }             
            }
        }

        private void maxPlayersBox_GotFocus(object sender, EventArgs e)
        {
            if (this.maxFirst)
            {
                this.maxPlayersBox.Clear();
                this.maxFirst = false;
            }
        }

        private void questionCountBox_GotFocus(object sender, EventArgs e)
        {
            if (this.numberFirst)
            {
                this.questionCountBox.Clear();
                this.numberFirst = false;
            }
        }

        private void answerTimeBox_GotFocus(object sender, EventArgs e)
        {
            if (this.timeFirst)
            {
                this.answerTimeBox.Clear();
                this.timeFirst = false;
            }
        }
    }
}
