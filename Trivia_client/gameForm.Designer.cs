﻿namespace Trivia_client
{
    partial class gameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gameForm));
            this.exitButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.Question = new System.Windows.Forms.Label();
            this.send = new System.Windows.Forms.Button();
            this.questionCount = new System.Windows.Forms.Label();
            this.correctAnswers = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timePerQuestion = new System.Windows.Forms.Label();
            this.answer1 = new System.Windows.Forms.RadioButton();
            this.answer2 = new System.Windows.Forms.RadioButton();
            this.answer3 = new System.Windows.Forms.RadioButton();
            this.answer4 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Snap ITC", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(12, 12);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 35);
            this.exitButton.TabIndex = 0;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Snap ITC", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(713, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 35);
            this.backButton.TabIndex = 1;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // Question
            // 
            this.Question.AutoSize = true;
            this.Question.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Question.Font = new System.Drawing.Font("Snap ITC", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Question.Location = new System.Drawing.Point(27, 62);
            this.Question.Name = "Question";
            this.Question.Size = new System.Drawing.Size(110, 25);
            this.Question.TabIndex = 2;
            this.Question.Text = "Question";
            // 
            // send
            // 
            this.send.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.send.Location = new System.Drawing.Point(275, 355);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(113, 42);
            this.send.TabIndex = 7;
            this.send.Text = "send";
            this.send.UseVisualStyleBackColor = true;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // questionCount
            // 
            this.questionCount.AutoSize = true;
            this.questionCount.Location = new System.Drawing.Point(29, 102);
            this.questionCount.Name = "questionCount";
            this.questionCount.Size = new System.Drawing.Size(75, 13);
            this.questionCount.TabIndex = 8;
            this.questionCount.Text = "questionCount";
            // 
            // correctAnswers
            // 
            this.correctAnswers.AutoSize = true;
            this.correctAnswers.Location = new System.Drawing.Point(337, 26);
            this.correctAnswers.Name = "correctAnswers";
            this.correctAnswers.Size = new System.Drawing.Size(80, 13);
            this.correctAnswers.TabIndex = 9;
            this.correctAnswers.Text = "correctAnswers";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timePerQuestion
            // 
            this.timePerQuestion.AutoSize = true;
            this.timePerQuestion.Location = new System.Drawing.Point(614, 26);
            this.timePerQuestion.Name = "timePerQuestion";
            this.timePerQuestion.Size = new System.Drawing.Size(29, 13);
            this.timePerQuestion.TabIndex = 10;
            this.timePerQuestion.Text = "timer";
            // 
            // answer1
            // 
            this.answer1.Location = new System.Drawing.Point(288, 100);
            this.answer1.Name = "answer1";
            this.answer1.Size = new System.Drawing.Size(100, 30);
            this.answer1.TabIndex = 11;
            this.answer1.TabStop = true;
            this.answer1.Text = "radioButton1";
            this.answer1.UseVisualStyleBackColor = true;
            // 
            // answer2
            // 
            this.answer2.Location = new System.Drawing.Point(288, 160);
            this.answer2.Name = "answer2";
            this.answer2.Size = new System.Drawing.Size(100, 30);
            this.answer2.TabIndex = 12;
            this.answer2.TabStop = true;
            this.answer2.Text = "radioButton2";
            this.answer2.UseVisualStyleBackColor = true;
            // 
            // answer3
            // 
            this.answer3.Location = new System.Drawing.Point(288, 220);
            this.answer3.Name = "answer3";
            this.answer3.Size = new System.Drawing.Size(100, 30);
            this.answer3.TabIndex = 13;
            this.answer3.TabStop = true;
            this.answer3.Text = "radioButton3";
            this.answer3.UseVisualStyleBackColor = true;
            // 
            // answer4
            // 
            this.answer4.Location = new System.Drawing.Point(288, 280);
            this.answer4.Name = "answer4";
            this.answer4.Size = new System.Drawing.Size(100, 30);
            this.answer4.TabIndex = 14;
            this.answer4.TabStop = true;
            this.answer4.Text = "radioButton4";
            this.answer4.UseVisualStyleBackColor = true;
            // 
            // gameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.answer4);
            this.Controls.Add(this.answer2);
            this.Controls.Add(this.answer1);
            this.Controls.Add(this.timePerQuestion);
            this.Controls.Add(this.correctAnswers);
            this.Controls.Add(this.questionCount);
            this.Controls.Add(this.send);
            this.Controls.Add(this.Question);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.answer3);
            this.MaximizeBox = false;
            this.Name = "gameForm";
            this.Text = "gameForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label Question;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.Label questionCount;
        private System.Windows.Forms.Label correctAnswers;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label timePerQuestion;
        private System.Windows.Forms.RadioButton answer1;
        private System.Windows.Forms.RadioButton answer2;
        private System.Windows.Forms.RadioButton answer3;
        private System.Windows.Forms.RadioButton answer4;
    }
}