﻿namespace Trivia_client
{
    partial class SignUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignUpForm));
            this.backButton = new System.Windows.Forms.Button();
            this.SignUp = new System.Windows.Forms.Label();
            this.usernameBox = new System.Windows.Forms.TextBox();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.EmailBox = new System.Windows.Forms.TextBox();
            this.phoneNumberBox = new System.Windows.Forms.TextBox();
            this.addressBox = new System.Windows.Forms.TextBox();
            this.birthDateBox = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.errorBox = new System.Windows.Forms.TextBox();
            this.exitButton = new System.Windows.Forms.Button();
            this.showPasswordButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.backButton.Location = new System.Drawing.Point(713, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 35);
            this.backButton.TabIndex = 0;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // SignUp
            // 
            this.SignUp.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.SignUp.Font = new System.Drawing.Font("Vladimir Script", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignUp.Location = new System.Drawing.Point(115, 12);
            this.SignUp.Name = "SignUp";
            this.SignUp.Size = new System.Drawing.Size(575, 131);
            this.SignUp.TabIndex = 1;
            this.SignUp.Text = "Sign up";
            this.SignUp.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // usernameBox
            // 
            this.usernameBox.BackColor = System.Drawing.SystemColors.Window;
            this.usernameBox.Location = new System.Drawing.Point(126, 157);
            this.usernameBox.Name = "usernameBox";
            this.usernameBox.Size = new System.Drawing.Size(158, 20);
            this.usernameBox.TabIndex = 7;
            this.usernameBox.Text = "Enter username";
            this.usernameBox.GotFocus += new System.EventHandler(this.usernameBox_GotFocus);
            // 
            // passwordBox
            // 
            this.passwordBox.Location = new System.Drawing.Point(126, 187);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.Size = new System.Drawing.Size(158, 20);
            this.passwordBox.TabIndex = 8;
            this.passwordBox.Text = "Enter password";
            this.passwordBox.GotFocus += new System.EventHandler(this.passwordBox_GotFocus);
            // 
            // EmailBox
            // 
            this.EmailBox.Location = new System.Drawing.Point(126, 217);
            this.EmailBox.Name = "EmailBox";
            this.EmailBox.Size = new System.Drawing.Size(158, 20);
            this.EmailBox.TabIndex = 9;
            this.EmailBox.Text = "Enter Email";
            this.EmailBox.GotFocus += new System.EventHandler(this.EmailBox_GotFocus);
            // 
            // phoneNumberBox
            // 
            this.phoneNumberBox.Location = new System.Drawing.Point(126, 247);
            this.phoneNumberBox.Name = "phoneNumberBox";
            this.phoneNumberBox.Size = new System.Drawing.Size(158, 20);
            this.phoneNumberBox.TabIndex = 10;
            this.phoneNumberBox.Text = "Enter phone number";
            this.phoneNumberBox.GotFocus += new System.EventHandler(this.phoneNumberBox_GotFocus);
            // 
            // addressBox
            // 
            this.addressBox.Location = new System.Drawing.Point(126, 277);
            this.addressBox.Name = "addressBox";
            this.addressBox.Size = new System.Drawing.Size(158, 20);
            this.addressBox.TabIndex = 11;
            this.addressBox.Text = "Enter Address";
            this.addressBox.GotFocus += new System.EventHandler(this.addressBox_GotFocus);
            // 
            // birthDateBox
            // 
            this.birthDateBox.Location = new System.Drawing.Point(126, 307);
            this.birthDateBox.Name = "birthDateBox";
            this.birthDateBox.Size = new System.Drawing.Size(158, 20);
            this.birthDateBox.TabIndex = 12;
            this.birthDateBox.Text = "Enter birth date";
            this.birthDateBox.GotFocus += new System.EventHandler(this.birthDateBox_GotFocus);
            // 
            // submitButton
            // 
            this.submitButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.submitButton.Location = new System.Drawing.Point(348, 370);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(100, 40);
            this.submitButton.TabIndex = 13;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // errorBox
            // 
            this.errorBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.errorBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.errorBox.Font = new System.Drawing.Font("David", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorBox.ForeColor = System.Drawing.Color.Red;
            this.errorBox.Location = new System.Drawing.Point(128, 95);
            this.errorBox.Name = "errorBox";
            this.errorBox.Size = new System.Drawing.Size(544, 21);
            this.errorBox.TabIndex = 14;
            this.errorBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.SystemColors.MenuBar;
            this.exitButton.Font = new System.Drawing.Font("Snap ITC", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(12, 12);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 35);
            this.exitButton.TabIndex = 15;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // showPasswordButton
            // 
            this.showPasswordButton.BackColor = System.Drawing.Color.Red;
            this.showPasswordButton.Font = new System.Drawing.Font("Guttman Vilna", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.showPasswordButton.Location = new System.Drawing.Point(290, 188);
            this.showPasswordButton.Name = "showPasswordButton";
            this.showPasswordButton.Size = new System.Drawing.Size(67, 19);
            this.showPasswordButton.TabIndex = 18;
            this.showPasswordButton.Text = "Show";
            this.showPasswordButton.UseVisualStyleBackColor = false;
            this.showPasswordButton.Click += new System.EventHandler(this.showPasswordButton_Click);
            // 
            // SignUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.showPasswordButton);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.errorBox);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.birthDateBox);
            this.Controls.Add(this.addressBox);
            this.Controls.Add(this.phoneNumberBox);
            this.Controls.Add(this.EmailBox);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.usernameBox);
            this.Controls.Add(this.SignUp);
            this.Controls.Add(this.backButton);
            this.MaximizeBox = false;
            this.Name = "SignUpForm";
            this.Text = "SignUpForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label SignUp;
        private System.Windows.Forms.TextBox usernameBox;
        private System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.TextBox EmailBox;
        private System.Windows.Forms.TextBox phoneNumberBox;
        private System.Windows.Forms.TextBox addressBox;
        private System.Windows.Forms.TextBox birthDateBox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.TextBox errorBox;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button showPasswordButton;
    }
}