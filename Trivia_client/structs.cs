﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;
namespace Trivia_client
{  
    class General
    {
        public General()
        { 
        }
        public JObject SendAndRecieve(Socket socket, string messageCode, string json)
        {
            string jsonLengthBytes = intToBin(json.Length);
            string fullMsg = messageCode + jsonLengthBytes + json;
            byte[] debug = Encoding.ASCII.GetBytes(fullMsg);
            socket.Send(debug);
            byte[] messageReceived = new byte[10000];
            int byteRecv = socket.Receive(messageReceived);
            string text = Encoding.ASCII.GetString(messageReceived);
            string recvBinString = "";

            string recvServerStatus = binaryToString(text);
            // Cut code and length message.
            for (int i = recvServerStatus.IndexOf('{'); i < recvServerStatus.Length; i++)
            {
                recvBinString += recvServerStatus[i];
            }
            JObject recvJson = JObject.Parse(recvBinString);
            return recvJson;
        }

        public string intToBin(int length)
        {
            string jsonLengthBytes = "";
            byte[] bytes = BitConverter.GetBytes(length);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
            for (int i = 0; i < bytes.Length; i++)
            {
                jsonLengthBytes += Convert.ToString(bytes[i], 2).PadLeft(8, '0');
            }
            return jsonLengthBytes;
        }

        public string binaryToString(string data)
        {
            List<Byte> byteList = new List<Byte>();

            for (int i = 0; i < data.Length && data[i] != '\0'; i += 8)
            {
                byteList.Add(Convert.ToByte(data.Substring(i, 8), 2));
            }
            return Encoding.ASCII.GetString(byteList.ToArray());
        }
    }
    
    public struct SignupRequest
    {
        public SignupRequest(string username, string password, string email, string address, string phoneNumber, string birthDate)
        {
            this.username = username;
            this.password = password;
            this.email = email;
            this.address = address;
            this.phoneNumber = phoneNumber;
            this.birthDate = birthDate;
        }
        public string username;
        public string password;
        public string email;
        public string address;
        public string phoneNumber;
        public string birthDate;
    };

    public struct SigninRequest
    {
        public SigninRequest(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
        public string username;
        public string password;
    };

    public struct RoomData
    {
        public RoomData(string roomName, int maxPlayers, int questionCount, int answerTimeout)
        {
            this.roomName = roomName;
            this.maxPlayers = maxPlayers;
            this.questionCount = questionCount;
            this.answerTimeout = answerTimeout;
        }
        public string roomName;
        public int maxPlayers;
        public int questionCount;
        public int answerTimeout;    
    };

    public struct JoinRoomRequest
    {
        public JoinRoomRequest(int roomId)
        {
            this.roomId = roomId;
        }
        public int roomId;
    };

    public struct SubmitAnswerRequest
    {
        public SubmitAnswerRequest(string answerSelected)
        {
            this.answerSelected = answerSelected;
        }
        public string answerSelected;
    };

}
