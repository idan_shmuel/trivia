﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;
namespace Trivia_client
{
    public partial class MenuForm : Form
    {
        private Socket socket;
        private General general;
        private string username;
        public MenuForm(string username, Socket socket)
        {
            InitializeComponent();
            this.welcomeLabel.Text = "Hello " + username;
            this.username = username;
            this.socket = socket;
            this.general = new General();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            //logout code
            string messageCode = "00000011";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            int statusCode = Convert.ToInt32(recvJson["status"].ToString());
            if (statusCode == 1)
            {
                Application.Exit();
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }

        private void createRoomButton_Click(object sender, EventArgs e)
        {
            CreateRoomForm createRoom = new CreateRoomForm(this.socket, this.username);
            this.Hide();
            createRoom.ShowDialog();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            //logout code
            string messageCode = "00000011";
            string json = "";
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            int statusCode = Convert.ToInt32(recvJson["status"].ToString());
            if (statusCode == 1)
            { 
                this.Close();
                SignInForm signInForm = new SignInForm(this.socket);
                this.Hide();
                signInForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }

        private void joinRoomButton_Click(object sender, EventArgs e)
        {
            JoinRoomForm joinRoom = new JoinRoomForm(this.socket, this.username);
            this.Hide();
            joinRoom.ShowDialog();
        }

        private void statisticsButton_Click(object sender, EventArgs e)
        {
            StatisticsForm statisticsForm = new StatisticsForm(this.socket, this.username);
            this.Hide();
            statisticsForm.ShowDialog();
        }
    }
}
