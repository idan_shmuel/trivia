﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class StatisticsForm : Form
    {
        private Socket socket;
        private General general;
        private string username;
        public StatisticsForm(Socket socket, string username)
        {
            InitializeComponent();
            this.socket = socket;
            this.username = username;
            this.general = new General();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuForm menuForm = new MenuForm(this.username, this.socket);
            this.Hide();
            menuForm.ShowDialog();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void myStatisticsButton_Click(object sender, EventArgs e)
        {
            MyStatisticsForm myStatisticsForm = new MyStatisticsForm(this.socket, this.username);
            this.Hide();
            myStatisticsForm.ShowDialog();
        }

        private void topFiveButton_Click(object sender, EventArgs e)
        {
            TopFiveForm topFiveForm = new TopFiveForm(this.socket, this.username);
            this.Hide();
            topFiveForm.ShowDialog();
        }
    }
}
