﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace Trivia_client
{
    public partial class SignInForm : Form
    {
        private bool userFirst, passwordFirst, passwordChar;
        private Socket socket;
        private General general;

        public SignInForm(Socket socket)
        {
            InitializeComponent();
            this.socket = socket;
            this.userFirst = true;
            this.passwordFirst = true;
            this.passwordChar = false;
            this.general = new General();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            MainForm mainForm = new MainForm();
            this.Hide();
            mainForm.ShowDialog();
        }

        private void usernameBox_GotFocus(object sender, EventArgs e)
        {
            if (this.userFirst)
            {
                this.usernameBox.Clear();
                this.userFirst = false;
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            errorBox.Clear();
            SigninRequest signinRequest = new SigninRequest(usernameBox.Text, passwordBox.Text);

            //signup code
            string messageCode = "00000001";
            string json = JsonConvert.SerializeObject(signinRequest);
            JObject recvJson = this.general.SendAndRecieve(this.socket, messageCode, json);
            if (recvJson["status"] != null)
            {
                int statusCode = Convert.ToInt32(recvJson["status"].ToString());
                switch (statusCode)
                {
                    case 1:
                        MenuForm menuForm = new MenuForm(this.usernameBox.Text, this.socket);
                        this.Hide();
                        menuForm.ShowDialog();
                        break;
                    case 2:
                        errorBox.Text = "Error! User doesn't exist.";
                        break;
                    case 3:
                        errorBox.Text = "Error! Username and password doesn't match.";
                        break;
                    case 4:
                        errorBox.Text = "Error! User already connected.";
                        break;

                }
            }
            else
            {
                MessageBox.Show("Server ERROR.");
            }
        }

        private void passwordBox_GotFocus(object sender, EventArgs e)
        {
            if (this.passwordFirst)
            {
                this.passwordBox.Clear();
                this.passwordBox.PasswordChar = '*';
                this.passwordChar = true;
                this.passwordFirst = false;
            }
        }
        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void showPasswordButton_Click(object sender, EventArgs e)
        {
            if(this.passwordChar)
            {
                this.passwordBox.PasswordChar = '\0';
                this.passwordChar = false;
                this.showPasswordButton.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                this.passwordBox.PasswordChar = '*';
                this.passwordChar = true;
                this.showPasswordButton.BackColor = System.Drawing.Color.Red;
            }
            
        }
    }
}
