﻿namespace Trivia_client
{
    partial class CreateRoomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitButton = new System.Windows.Forms.Button();
            this.enterDetailsLabel = new System.Windows.Forms.Label();
            this.roomNameBox = new System.Windows.Forms.TextBox();
            this.maxPlayersBox = new System.Windows.Forms.TextBox();
            this.questionCountBox = new System.Windows.Forms.TextBox();
            this.answerTimeBox = new System.Windows.Forms.TextBox();
            this.backButton = new System.Windows.Forms.Button();
            this.submitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.SystemColors.MenuBar;
            this.exitButton.Font = new System.Drawing.Font("Snap ITC", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(12, 12);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 35);
            this.exitButton.TabIndex = 3;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // enterDetailsLabel
            // 
            this.enterDetailsLabel.AutoSize = true;
            this.enterDetailsLabel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.enterDetailsLabel.Font = new System.Drawing.Font("MV Boli", 30.75F);
            this.enterDetailsLabel.Location = new System.Drawing.Point(200, 56);
            this.enterDetailsLabel.Name = "enterDetailsLabel";
            this.enterDetailsLabel.Size = new System.Drawing.Size(387, 54);
            this.enterDetailsLabel.TabIndex = 4;
            this.enterDetailsLabel.Text = "Enter room details";
            // 
            // roomNameBox
            // 
            this.roomNameBox.Location = new System.Drawing.Point(200, 120);
            this.roomNameBox.Name = "roomNameBox";
            this.roomNameBox.Size = new System.Drawing.Size(158, 20);
            this.roomNameBox.TabIndex = 5;
            this.roomNameBox.Text = "Enter room name";
            this.roomNameBox.GotFocus += new System.EventHandler(this.roomNameBox_GotFocus);
            // 
            // maxPlayersBox
            // 
            this.maxPlayersBox.Location = new System.Drawing.Point(200, 145);
            this.maxPlayersBox.Name = "maxPlayersBox";
            this.maxPlayersBox.Size = new System.Drawing.Size(158, 20);
            this.maxPlayersBox.TabIndex = 6;
            this.maxPlayersBox.Text = "Enter max players";
            this.maxPlayersBox.GotFocus += new System.EventHandler(this.maxPlayersBox_GotFocus);
            // 
            // questionCountBox
            // 
            this.questionCountBox.Location = new System.Drawing.Point(200, 170);
            this.questionCountBox.Name = "questionCountBox";
            this.questionCountBox.Size = new System.Drawing.Size(158, 20);
            this.questionCountBox.TabIndex = 7;
            this.questionCountBox.Text = "Enter number of questions";
            this.questionCountBox.GotFocus += new System.EventHandler(this.questionCountBox_GotFocus);
            // 
            // answerTimeBox
            // 
            this.answerTimeBox.Location = new System.Drawing.Point(200, 195);
            this.answerTimeBox.Name = "answerTimeBox";
            this.answerTimeBox.Size = new System.Drawing.Size(158, 20);
            this.answerTimeBox.TabIndex = 8;
            this.answerTimeBox.Text = "Enter time per question";
            this.answerTimeBox.GotFocus += new System.EventHandler(this.answerTimeBox_GotFocus);
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.backButton.Location = new System.Drawing.Point(713, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 35);
            this.backButton.TabIndex = 9;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // submitButton
            // 
            this.submitButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.submitButton.Location = new System.Drawing.Point(348, 285);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(100, 40);
            this.submitButton.TabIndex = 14;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // CreateRoomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Trivia_client.Properties.Resources.signUpScreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.answerTimeBox);
            this.Controls.Add(this.questionCountBox);
            this.Controls.Add(this.maxPlayersBox);
            this.Controls.Add(this.roomNameBox);
            this.Controls.Add(this.enterDetailsLabel);
            this.Controls.Add(this.exitButton);
            this.MaximizeBox = false;
            this.Name = "CreateRoomForm";
            this.Text = "CreateRoomForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label enterDetailsLabel;
        private System.Windows.Forms.TextBox roomNameBox;
        private System.Windows.Forms.TextBox maxPlayersBox;
        private System.Windows.Forms.TextBox questionCountBox;
        private System.Windows.Forms.TextBox answerTimeBox;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button submitButton;
    }
}