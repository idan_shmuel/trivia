﻿namespace Trivia_client
{
    partial class StatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.myStatisticsButton = new System.Windows.Forms.Button();
            this.topFiveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.SystemColors.MenuBar;
            this.exitButton.Font = new System.Drawing.Font("Snap ITC", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(12, 12);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 35);
            this.exitButton.TabIndex = 6;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Snap ITC", 15F);
            this.backButton.Location = new System.Drawing.Point(713, 12);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 35);
            this.backButton.TabIndex = 7;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // myStatisticsButton
            // 
            this.myStatisticsButton.BackColor = System.Drawing.SystemColors.MenuBar;
            this.myStatisticsButton.Font = new System.Drawing.Font("Vladimir Script", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myStatisticsButton.Location = new System.Drawing.Point(252, 139);
            this.myStatisticsButton.Name = "myStatisticsButton";
            this.myStatisticsButton.Size = new System.Drawing.Size(296, 78);
            this.myStatisticsButton.TabIndex = 8;
            this.myStatisticsButton.Text = "My Statistics";
            this.myStatisticsButton.UseVisualStyleBackColor = false;
            this.myStatisticsButton.Click += new System.EventHandler(this.myStatisticsButton_Click);
            // 
            // topFiveButton
            // 
            this.topFiveButton.BackColor = System.Drawing.SystemColors.MenuBar;
            this.topFiveButton.Font = new System.Drawing.Font("Vladimir Script", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topFiveButton.Location = new System.Drawing.Point(252, 241);
            this.topFiveButton.Name = "topFiveButton";
            this.topFiveButton.Size = new System.Drawing.Size(296, 78);
            this.topFiveButton.TabIndex = 9;
            this.topFiveButton.Text = "Top 5";
            this.topFiveButton.UseVisualStyleBackColor = false;
            this.topFiveButton.Click += new System.EventHandler(this.topFiveButton_Click);
            // 
            // StatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Trivia_client.Properties.Resources.signUpScreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.topFiveButton);
            this.Controls.Add(this.myStatisticsButton);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.exitButton);
            this.Name = "StatisticsForm";
            this.Text = "StatisticsForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button myStatisticsButton;
        private System.Windows.Forms.Button topFiveButton;
    }
}