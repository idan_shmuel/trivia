import socket
import json
SERVER_IP = "127.0.0.1"
SERVER_PORT = 5353

def open_sock():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    send(sock)
    sock.close()

def send(sock):
    try: 
        client_input = ""	
        server_msg = ""
        client_input = ""
        client_msg = ""
        username = ""
        password = ""
        mail = ""
        server_msg = ""
        address = ""
        phoneNumber = ""
        birthDate = ""
        while server_msg != "EXIT":
            #server_msg = sock.recv(1024)
            #server_msg = server_msg.decode()
           # print(server_msg)
            while(client_input != "Login" and client_input != "Signup"):
                client_input = input()
            if client_input == "Login" or client_input == "Signup":
                username = input("Enter username:")
                password = input("Enter password:")

            if client_input == "Signup":
                client_msg = "00000010"
                mail = input("Enter mail: ")
                address = input("Enter address: ")
                phoneNumber = input("Enter phone number: ")
                birthDate = input("Enter birth date: ")		
                x = {'username': username, 'password': password, 'mail': mail, 'address': address, 'phoneNumber': phoneNumber, 'birthDate': birthDate}
                jsonMsg = json.dumps(x)

            if client_input == "Login":
                client_msg = "00000001"
                x = {'username': username, 'password': password}
                jsonMsg = json.dumps(x)

            client_msg += bin(len(jsonMsg))[2:].zfill(32)
            client_msg += jsonMsg
            print(client_msg.encode())
            sock.sendall(client_msg.encode())
            server_msg = sock.recv(1024)
            server_msg = server_msg.decode()
            print(server_msg)
            client_input = ""		    

    except Exception as e:
        print("ERROR\n", e)
        sock.close()

def main():
    open_sock()

if __name__ == "__main__":
    main()
