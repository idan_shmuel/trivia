#include "Room.h"

Room::Room()
{
	this->m_metadata = new RoomData();
}

Room::~Room()
{
	delete this->m_metadata;
}

void Room::startsGame()
{
	this->m_metadata->isActive = 1;
}

void Room::addUser(LoggedUser* newUser)
{
	this->m_users.push_back(newUser);
}

void Room::removeUser(LoggedUser* user)
{
	for (int i = 0; i < this->m_users.size(); i++)
	{
		if (this->m_users[i]->getUsername() == user->getUsername())
		{
			this->m_users.erase(this->m_users.begin() + i);
		}
	}
}

std::vector<LoggedUser*> Room::getAllUsers()
{
	return this->m_users;
}

void Room::setData(RoomData* newData)
{
	this->m_metadata = newData;
}

void Room::setUsers(std::vector<LoggedUser*> users)
{
	this->m_users = users;
}

RoomData* Room::getRoomData()
{
	return this->m_metadata;
}
