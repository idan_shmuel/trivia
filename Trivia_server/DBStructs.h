#pragma once
#include "pch.h"

typedef struct QuestionStruct
{
	std::string question;
	std::string correctAnswer;
	std::string incorrectAnswers[3];
}QuestionStruct;