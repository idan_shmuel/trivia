#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(LoggedUser* loggedUser, RequestHandlerFactory* handleFactory) : IRequestHandler()
{
	this->m_handlerFactory = handleFactory;
	this->m_loggedUser = loggedUser;
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.messageCode == LOGOUT_CODE || requestInfo.messageCode == GET_ROOMS_CODE || requestInfo.messageCode == GET_PLAYERS_IN_ROOM_CODE || requestInfo.messageCode == JOIN_ROOM_CODE || requestInfo.messageCode == CREATE_ROOM_CODE || requestInfo.messageCode == HIGH_SCORE_CODE || requestInfo.messageCode == PLAYER_STATISTICS_CODE);
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestResult;
	ErrorResponse errorResponse;
	unsigned char* fullMessage = {};

	try
	{
		switch (requestInfo.messageCode)
		{
		case LOGOUT_CODE:
			return handleLogoutRequest(requestInfo);

		case GET_ROOMS_CODE:
			return handleGetRoomsRequest(requestInfo);

		case GET_PLAYERS_IN_ROOM_CODE:
			return handleGetPlayersInRoomRequest(requestInfo);

		case JOIN_ROOM_CODE:
			return handleJoinRoomRequest(requestInfo);

		case CREATE_ROOM_CODE:
			return handleCreateRoomRequest(requestInfo);

		case HIGH_SCORE_CODE:
			return handleHighScoreRequest(requestInfo);
		
		case PLAYER_STATISTICS_CODE:
			return handleGetStatistics(requestInfo);

		default:
			break;
		}
	}
	catch (std::exception e)
	{
		std::cout << "Error: " << e.what() << std::endl;
		requestResult.newHandler = nullptr;
		errorResponse.message = "ERROR";
		fullMessage = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.buffer = fullMessage;
		return requestResult;
	}
}

RequestResult MenuRequestHandler::handleLogoutRequest(RequestInfo requestInfo)
{
	LogoutResponse logoutResponse;
	RequestResult requestResult;
	bool isSuccess = this->m_handlerFactory->getLoginManager()->logout(this->m_loggedUser->getUsername());
	if (isSuccess)
	{
		logoutResponse.status = SUCCESS_CODE;
		requestResult.newHandler = this->m_handlerFactory->createLoginRequestHandler(this->m_handlerFactory);
	}
	else
	{
		logoutResponse.status = ERROR_CODE;
		requestResult.newHandler = NULL;
	}
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(logoutResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult MenuRequestHandler::handleGetRoomsRequest(RequestInfo requestInfo)
{
	GetRoomsResponse getRoomsResponse;
	RequestResult requestResult;
	std::vector<Room*> rooms = this->m_handlerFactory->getRoomManager()->getRooms();
	if (!rooms.empty())
	{
		getRoomsResponse.status = SUCCESS_CODE;
		for (int i = 0; i < rooms.size(); i++)
		{
			getRoomsResponse.rooms.push_back(*rooms[i]->getRoomData());
		}
	}
	else
	{
		getRoomsResponse.status = ERROR_CODE;
	}
	requestResult.newHandler = NULL;
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(getRoomsResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult MenuRequestHandler::handleGetPlayersInRoomRequest(RequestInfo requestInfo)
{
	GetPlayersInRoomResponse getPlayersInRoomResponse;
	RequestResult requestResult;
	GetPlayersInRoomRequest getPlayersInRoomRequest = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(requestInfo.buffer);
	std::vector<Room*> rooms = this->m_handlerFactory->getRoomManager()->getRooms();
	std::vector<LoggedUser*> loggedUsers;
	for (int i = 0; i < rooms.size(); i++)
	{
		if (rooms[i]->getRoomData()->id == getPlayersInRoomRequest.roomId)
		{
			loggedUsers = rooms[i]->getAllUsers();
		}
	}
	if (!loggedUsers.empty())
	{
		getPlayersInRoomResponse.status = SUCCESS_CODE;
	}
	else
	{
		getPlayersInRoomResponse.status = ERROR_CODE;
	}
	requestResult.newHandler = NULL;
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(getPlayersInRoomResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult MenuRequestHandler::handleJoinRoomRequest(RequestInfo requestInfo)
{
	JoinRoomResponse joinRoomResponse;
	RequestResult requestResult;
	JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(requestInfo.buffer);
	std::vector<Room*> rooms = this->m_handlerFactory->getRoomManager()->getRooms();
	Room* room = nullptr;
	bool isRoomExist = false;
	for (int i = 0; i < rooms.size(); i++)
	{
		if (rooms[i]->getRoomData()->id == joinRoomRequest.roomId)
		{
			rooms[i]->addUser(this->m_loggedUser);
			isRoomExist = true;
			room = rooms[i];
		}
	}
	if (isRoomExist)
	{
		joinRoomResponse.status = SUCCESS_CODE;
		requestResult.newHandler = this->m_handlerFactory->createRoomMemberRequestHandler(room, this->m_loggedUser, this->m_handlerFactory->getRoomManager(), this->m_handlerFactory);
	}
	else
	{
		joinRoomResponse.status = ERROR_CODE;
		requestResult.newHandler = NULL;
	}
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(joinRoomResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult MenuRequestHandler::handleCreateRoomRequest(RequestInfo requestInfo)
{
	CreateRoomResponse createRoomResponse;
	RequestResult requestResult;
	CreateRoomRequest createRoomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(requestInfo.buffer);
	RoomData* roomData = new RoomData(0, createRoomRequest.answerTimeout, 0, createRoomRequest.maxUsers, createRoomRequest.questionCount, createRoomRequest.roomName);
	this->m_handlerFactory->getRoomManager()->createRoom(this->m_loggedUser, roomData);
	createRoomResponse.status = SUCCESS_CODE;
	requestResult.newHandler = this->m_handlerFactory->createRoomAdminRequestHandler(this->m_handlerFactory->getRoomManager()->getRooms().back(), this->m_loggedUser, this->m_handlerFactory->getRoomManager(), this->m_handlerFactory);
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(createRoomResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult MenuRequestHandler::handleHighScoreRequest(RequestInfo requestInfo)
{
	TopFiveResponse topFiveResponse;
	RequestResult requestResult;
	std::vector<std::pair<std::string, double>> topFive = this->m_handlerFactory->getStatisticsManager()->getTopFivePlayers();
	if (!topFive.empty())
	{
		topFiveResponse.status = SUCCESS_CODE;
		topFiveResponse.topFive = topFive;
	}
	else
	{
		topFiveResponse.status = ERROR_CODE;
	}
	requestResult.newHandler = NULL;
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(topFiveResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult MenuRequestHandler::handleGetStatistics(RequestInfo requestInfo)
{
	PlayerStatisticsResponse playerStatisticsResponse;
	RequestResult requestResult;
	std::map<std::string, double> userStatistics = this->m_handlerFactory->getStatisticsManager()->getUserStatistics(this->m_loggedUser->getUsername());	
	if (!userStatistics.empty())
	{
		playerStatisticsResponse.status = SUCCESS_CODE;
		playerStatisticsResponse.username = this->m_loggedUser->getUsername();
		playerStatisticsResponse.averageAnswerTime = userStatistics["playerAverageAnswerTime"];
		playerStatisticsResponse.correctAnswerCount = userStatistics["numOfCorrectAnswers"];
		playerStatisticsResponse.numOfPlayerGames = userStatistics["numOfPlayerGames"];
		playerStatisticsResponse.totalAnswerCount = userStatistics["numOfTotalAnswers"];

	}
	else
	{
		playerStatisticsResponse.status = ERROR_CODE;
	}
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(playerStatisticsResponse);
	requestResult.newHandler = NULL;
	requestResult.buffer = fullMessage;
	return requestResult;
}

