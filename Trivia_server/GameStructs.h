#pragma once
#include "DBStructs.h"
#include "Question.h"

typedef struct GameData
{
	GameData(Question currentQuestion, unsigned int correctAnswersCount, unsigned int wrongAnswersCount, unsigned int averageAnswerTime, unsigned int currentQuestionNum)
	{
		this->currentQuestion = Question(currentQuestion.getQuestion(), currentQuestion.getPossibleAnswers());
		this->correctAnswersCount = correctAnswersCount;
		this->wrongAnswersCount = wrongAnswersCount;
		this->averageAnswerTime = averageAnswerTime;
		this->currentQuestionNum = currentQuestionNum;
	}
	GameData()
	{
		this->currentQuestion = Question("", std::vector<std::string>());
		this->correctAnswersCount = 0;
		this->wrongAnswersCount = 0;
		this->averageAnswerTime = 0;
		this->currentQuestionNum = 0;
	}
	Question currentQuestion;
	unsigned int correctAnswersCount;
	unsigned int wrongAnswersCount;
	unsigned int averageAnswerTime;
	unsigned int currentQuestionNum;
}GameData;
