#pragma once
#include "pch.h"
#include "LoggedUser.h"
#include "RoomStructs.h"

class Room
{
private:
	RoomData* m_metadata;
	std::vector<LoggedUser*> m_users;

public:
	Room();
	~Room();
	void startsGame();
	void addUser(LoggedUser* newUser);
	void removeUser(LoggedUser* user);
	std::vector<LoggedUser*> getAllUsers();
	void setData(RoomData* newData);
	void setUsers(std::vector<LoggedUser*> users);
	RoomData* getRoomData();
};

