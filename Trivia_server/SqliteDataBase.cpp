#include "SqliteDataBase.h"

double SqliteDataBase::getSpecificStatistic(std::string username, std::string statisticName)
{
	double statistic;
	std::string sqlStatement = "SELECT " + statisticName + " FROM STATISTICS WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetStatistics, &statistic, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to get player statistic." << std::endl;
	}
	return statistic;
}

std::vector<Question> SqliteDataBase::getAllQuestions()
{
	std::vector<Question> questions;
	std::string sqlStatement = "SELECT * FROM QUESTIONS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetQuestions, &questions, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to get all questions" << std::endl;
	}
	return questions;
}

int SqliteDataBase::callbackGetUsers(void* data, int argc, char** argv, char** azColName)
{
	std::list<std::string>* usersList = (std::list<std::string>*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "username")
		{
			usersList->push_back(argv[i]);
		}
	}
	return 0;
}

int SqliteDataBase::callbackGetStatistics(void* data, int argc, char** argv, char** azColName)
{
	double* statistic = (double*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "playerAverageAnswerTime")
		{
			*statistic = atof(argv[i]);
		}
		else if (std::string(azColName[i]) == "numOfCorrectAnswers")
		{
			*statistic = atof(argv[i]);
		}
		else if (std::string(azColName[i]) == "numOfPlayerGames")
		{
			*statistic = atof(argv[i]);
		}
		else if (std::string(azColName[i]) == "numOfTotalAnswers")
		{
			*statistic = atof(argv[i]);
		}
		else if (std::string(azColName[i]) == "totalTimeForAnswer")
		{
			*statistic = atof(argv[i]);
		}
	}
	return 0;
}

int SqliteDataBase::callbackGetQuestions(void* data, int argc, char** argv, char** azColName)
{
	std::vector<Question>* questions = (std::vector<Question>*)data;
	std::vector<std::string> possibleAnswers;
	std::string question;
	std::string answer1;
	std::string answer2;
	std::string answer3;
	std::string answer4;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "question")
		{
			question = argv[i];
		}
		else if (std::string(azColName[i]) == "correctAnswer")
		{
			answer1 = argv[i];
		}
		else if (std::string(azColName[i]) == "incorrectAnswer1")
		{
			answer2 = argv[i];
		}
		else if (std::string(azColName[i]) == "incorrectAnswer2")
		{
			answer3 = argv[i];
		}
		else if (std::string(azColName[i]) == "incorrectAnswer3")
		{
			answer4 = argv[i];
		}
	}
	possibleAnswers.push_back(answer1);
	possibleAnswers.push_back(answer2);
	possibleAnswers.push_back(answer3);
	possibleAnswers.push_back(answer4);
	questions->push_back(Question(question, possibleAnswers));
	return 0;
}

std::list<std::string> SqliteDataBase::getUsernames()
{
	std::list<std::string> usernamesList;
	std::string sqlStatement = "SELECT * FROM USERS;";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetUsers, &usernamesList, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to get all usernames" << std::endl;
	}
	return usernamesList;
}

SqliteDataBase::SqliteDataBase() : IDatabase()
{
	this->open();
}

SqliteDataBase::~SqliteDataBase()
{
	this->close();
}

bool SqliteDataBase::doesUserExist(std::string username)
{
	std::list<std::string> usersList;
	std::string sqlStatement = "SELECT * FROM USERS WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetUsers, &usersList, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to check if user exists" << std::endl;
	}
	return !usersList.empty();
}

bool SqliteDataBase::doesUserValid(std::string username)
{
	return !(username == "");
}

bool SqliteDataBase::doesPasswordMatch(std::string username, std::string password)
{
	std::list<std::string> usersList;
	std::string sqlStatement = "SELECT * FROM USERS WHERE USERNAME = '" + username + "' AND PASSWORD = '" + password + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), callbackGetUsers, &usersList, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to check if user exists" << std::endl;
	}
	return !usersList.empty();
}

void SqliteDataBase::addNewUser(std::string username, std::string password, std::string email, std::string address, std::string phoneNumber, std::string birthDate)
{
	std::string sqlStatement = "INSERT INTO USERS (username, password, email, address, phoneNumber, birthDate) VALUES('" + username + "', '" + password + "', '" + email + "', '" + address + "', '" + phoneNumber + "', '" + birthDate + "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to add user to the users table." << std::endl;
	}
	sqlStatement = "INSERT INTO STATISTICS (username) VALUES('" + username + "');";
	errMessage = nullptr;
	res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to add the user to the statistics table." << std::endl;
	}
}

bool SqliteDataBase::createTable(std::string sqlStatement)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to create table" << std::endl;
		return false;
	}
	return true;
}

void SqliteDataBase::addNewQuestion(std::string jsonQuestion)
{
	auto jsonMessage = nlohmann::json::parse(jsonQuestion.begin(), jsonQuestion.end());
	QuestionStruct question;
	question.question = jsonMessage["question"];
	question.correctAnswer = jsonMessage["correct_answer"];
	for (int i = 0; i < 3; i++)
	{
		question.incorrectAnswers[i] = jsonMessage["incorrect_answers"][i];
	}
	std::string sqlStatement = "INSERT INTO QUESTIONS (question, correctAnswer, incorrectAnswer1, incorrectAnswer2, incorrectAnswer3) VALUES('" + question.question + "', '" + question.correctAnswer + "', '" + question.incorrectAnswers[0] + "', '" + question.incorrectAnswers[1] + "', '" + question.incorrectAnswers[2] + "');";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to add question" << std::endl;
	}
}

void SqliteDataBase::updateTotalTimeForAnswer(std::string username, float totalTimeForAnswer)
{
	std::string sqlStatement = "UPDATE STATISTICS SET totalTimeForAnswer = totalTimeForAnswer + " + std::to_string(totalTimeForAnswer) + " WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to update total answer time for answer." << std::endl;
	}
}

void SqliteDataBase::updateUserAvgAnswerTime(std::string username, float playerAverageAnswerTime)
{
	std::string sqlStatement = "UPDATE STATISTICS SET playerAverageAnswerTime = " + std::to_string(playerAverageAnswerTime) + " WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to update user avarage answer time." << std::endl;
	}
}

void SqliteDataBase::increaseUserNumCorrectAns(std::string username)
{
	std::string sqlStatement = "UPDATE STATISTICS SET numOfCorrectAnswers = numOfCorrectAnswers + 1 WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to increas user number correct answer." << std::endl;
	}
}

void SqliteDataBase::increaseUserNumOfGames(std::string username)
{
	std::string sqlStatement = "UPDATE STATISTICS SET numOfPlayerGames = numOfPlayerGames + 1 WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to increase user number of games." << std::endl;
	}
}

void SqliteDataBase::increaseUserNumTotalAnswers(std::string username)
{
	std::string sqlStatement = "UPDATE STATISTICS SET numOfTotalAnswers = numOfTotalAnswers + 1 WHERE USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed to increase user number total answers." << std::endl;
	}
}

float SqliteDataBase::getPlayerAverageAnswerTime(std::string username)
{
	return getSpecificStatistic(username, "playerAverageAnswerTime");
}

int SqliteDataBase::getNumOfCorrectAnswers(std::string username)
{
	return getSpecificStatistic(username, "numOfCorrectAnswers");
}

int SqliteDataBase::getNumOfTotalAnswers(std::string username)
{
	return getSpecificStatistic(username, "numOfTotalAnswers");
}

int SqliteDataBase::getNumOfPlayerGames(std::string username)
{
	return getSpecificStatistic(username, "numOfPlayerGames");
}

float SqliteDataBase::getTotalTimeForAnswer(std::string username)
{
	return getSpecificStatistic(username, "totalTimeForAnswer");
}

bool SqliteDataBase::open()
{
	std::string dbFileName = "TriviaDataBase.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &this->_db);
	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	if (doesFileExist == -1)
	{
		//create users table
		res = createTable("create table users(username text primary key not null, password text not null, email text not null, address text not null, phoneNumber text not null, birthDate text not null);");
		if (!res)
		{
			return false;
		}

		//create questions table
		res = createTable("create table questions(question text primary key not null, correctAnswer text not null, incorrectAnswer1 text not null, incorrectAnswer2 text not null, incorrectAnswer3 text not null);");
		if (!res)
		{
			return false;
		}

		//create statistics table
		res = createTable("create table statistics(username TEXT PRIMARY KEY NOT NULL, playerAverageAnswerTime FLOAT DEFAULT 0 NOT NULL, numOfCorrectAnswers INT DEFAULT 0 NOT NULL, numOfPlayerGames INT DEFAULT 0 NOT NULL, numOfTotalAnswers INT DEFAULT 0 NOT NULL, totalTimeForAnswer FLOAT DEFAULT 0 NOT NULL);");
		if (!res)
		{
			return false;
		}

		std::ifstream file("questions.txt");
		if (file.is_open()) {
			std::string line;
			while (std::getline(file, line))
			{
				this->addNewQuestion(line);
			}
			file.close();
		}
	}

	return true;
}

void SqliteDataBase::close()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}
