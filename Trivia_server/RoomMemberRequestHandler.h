#pragma once
#include "IRequestHandler.h"
#include "Room.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "MenuRequestHandler.h"

class RoomMemberRequestHandler : public IRequestHandler
{
private:
	Room* m_room;
	LoggedUser* m_user;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFactory;
	RequestResult handleLeaveRoom(RequestInfo requestInfo);
	RequestResult handleGetRoomState(RequestInfo requestInfo);

public:
	RoomMemberRequestHandler(Room* room, LoggedUser* user, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo);
};

