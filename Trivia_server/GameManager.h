#pragma once
#include "pch.h"
#include "IDatabase.h"
#include "LoggedUser.h"
#include "SqliteDataBase.h"
#include "Game.h"
#include "Room.h"

class GameManager
{
private:
	SqliteDataBase* m_database;
	std::vector<Game*> m_games;

public:
	GameManager();
	~GameManager();
	Game* createGame(Room* room);
	void deleteGame(Game* game);
	Game* getCurrentGame(LoggedUser* user);
};

