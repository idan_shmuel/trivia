#pragma once
#include "pch.h"
#include "requestStructs.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

struct RequestResult;
struct RequestInfo;
class IRequestHandler
{
public:
	IRequestHandler();

	virtual bool isRequestRelevant(RequestInfo requestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo requestInfo) = 0;
};

