#pragma once
#include "pch.h"
#include "IDatabase.h"
#include "LoggedUser.h"
#include "SqliteDataBase.h"

class LoginManager
{
private:
	IDatabase* m_database;
	std::vector<LoggedUser*> m_loggedUsers;

public:
	LoginManager();
	~LoginManager();

	int signup(std::string username, std::string password, std::string email, std::string address, std::string phoneNumber, std::string birthDate);
	int login(std::string username, std::string password);
	bool logout(std::string username);

	bool doesPasswordValid(std::string password);
	bool doesEmailValid(std::string email);
	bool doesAddressValid(std::string address);
	bool doesphonenumberValid(std::string phoneNumber);
	bool doesBirthDateValid(std::string birthDate);
};

