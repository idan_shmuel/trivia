#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"


class GameRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* m_handlerFactory;
	LoggedUser* m_loggedUser;
	Game* m_game;
	clock_t m_tStart;
	float m_totalTimeForAnswer;
	RequestResult handleLeaveGameRequest(RequestInfo requestInfo);
	RequestResult handleGetQuestionRequest(RequestInfo requestInfo);
	RequestResult handleSubmitAnswerRequest(RequestInfo requestInfo);
	RequestResult handleGetGameResult(RequestInfo requestInfo);
	void static setAnswersForQuestion(int* alreadyWritten, Question question, GetQuestionResponse& getQuestionResponse);

public:
	GameRequestHandler(RequestHandlerFactory* handleFactory, LoggedUser* loggedUser, Game* game);
	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo);

};

