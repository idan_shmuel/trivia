#pragma once
#include "pch.h"
#include "RoomStructs.h"

typedef struct ErrorResponse
{
	std::string message;
}ErrorResponse;

typedef struct LoginResponse
{
	unsigned int status;
}LoginResponse;

typedef struct SignupResponse
{
	unsigned int status;
}SignupResponse;

typedef struct LogoutResponse
{
	unsigned int status;
}LogoutResponse;

typedef struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
}GetRoomsResponse;

typedef struct GetPlayersInRoomResponse
{
	unsigned int status;
	std::vector<std::string> rooms;
}GetPlayersInRoomResponse;

typedef struct TopFiveResponse
{
	unsigned int status;
	std::vector<std::pair<std::string, double>>  topFive;
}HighScoreResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;
}JoinRoomResponse;

typedef struct CreateRoomResponse
{
	unsigned int status;
}CreateRoomResponse;

typedef struct CloseRoomResponse
{
	unsigned int status;
}CloseRoomResponse;

typedef struct StartGameResponse
{
	unsigned int status;
}StartGameResponse;

typedef struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	float answerTimeout;
}GetRoomStateResponse;

typedef struct LeaveRoomResponse
{
	unsigned int status;
}LeaveRoomResponse;

typedef struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::map<unsigned int, std::string> answers;
}GetQuestionResponse;

typedef struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
}SubmitAnswerResponse;

typedef struct PlayerStatisticsResponse
{
	unsigned int status;
	std::string username;
	unsigned int numOfPlayerGames;
	unsigned int correctAnswerCount;
	unsigned int totalAnswerCount;
	float averageAnswerTime;
}PlayerStatisticsResponse;

typedef struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerStatisticsResponse> results;
}GetGameResultsResponse;

typedef struct LeaveGameResponse
{
	unsigned int status;
}LeaveGameResponse;