#pragma once
#include "LoginManager.h"
#include "IRequestHandler.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "GameManager.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
private:
	LoginManager* m_loginManager;
	RoomManager* m_roomManager;
	StatisticsManager* m_statisticsManager;
	GameManager* m_gameManager;

public:
	RequestHandlerFactory();
	~RequestHandlerFactory();
	LoginRequestHandler* createLoginRequestHandler(RequestHandlerFactory* handleFactory);
	MenuRequestHandler* createMenuRequestHandler(LoggedUser* loggedUser, RequestHandlerFactory* handleFactory);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(Room* room, LoggedUser* user, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(Room* room, LoggedUser* user, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	GameRequestHandler* createGameRequestHandler(RequestHandlerFactory* handleFactory, LoggedUser* user, Game* game);
	LoginManager* getLoginManager();
	RoomManager* getRoomManager();
	StatisticsManager* getStatisticsManager();
	GameManager* getGameManager();
	
};

