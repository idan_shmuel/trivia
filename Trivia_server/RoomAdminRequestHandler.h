#pragma once
#include "RoomMemberRequestHandler.h"

class RoomAdminRequestHandler : public IRequestHandler
{
private:
	Room* m_room;
	LoggedUser* m_user;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFactory;
	RequestResult handleCloseRoom(RequestInfo requestInfo);
	RequestResult handleStartGame(RequestInfo requestInfo);
	RequestResult handleGetRoomState(RequestInfo requestInfo);

public:
	RoomAdminRequestHandler(Room* room, LoggedUser* user, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo);
};

