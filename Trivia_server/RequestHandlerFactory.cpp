#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "GameRequestHandler.h"

RequestHandlerFactory::RequestHandlerFactory()
{
	this->m_loginManager = new LoginManager();
	this->m_roomManager = new RoomManager();
	this->m_statisticsManager = new StatisticsManager();
	this->m_gameManager = new GameManager();
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	delete this->m_loginManager;
	delete this->m_gameManager;
	delete this->m_statisticsManager;
	delete this->m_roomManager;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler(RequestHandlerFactory* handleFactory)
{
	return new LoginRequestHandler(handleFactory);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser* loggedUser, RequestHandlerFactory* handleFactory)
{
	return new MenuRequestHandler(loggedUser, handleFactory);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(Room* room, LoggedUser* user, RoomManager* roomManager, RequestHandlerFactory* handlerFactory)
{
	return new RoomAdminRequestHandler(room, user, roomManager, handlerFactory);
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(Room* room, LoggedUser* user, RoomManager* roomManager, RequestHandlerFactory* handlerFactory)
{
	return new RoomMemberRequestHandler(room, user, roomManager, handlerFactory);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(RequestHandlerFactory* handleFactory, LoggedUser* user, Game* game)
{
	return new GameRequestHandler(handleFactory, user, game);
}

LoginManager* RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}

RoomManager* RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}

StatisticsManager* RequestHandlerFactory::getStatisticsManager()
{
	return this->m_statisticsManager;
}

GameManager* RequestHandlerFactory::getGameManager()
{
	return this->m_gameManager;
}
