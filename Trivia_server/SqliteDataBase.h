#pragma once
#include "IDatabase.h"
#include "DBStructs.h"
#include "Question.h"

class SqliteDataBase : public IDatabase
{
public:
	SqliteDataBase();
	~SqliteDataBase();

	bool doesUserExist(std::string username);
	bool doesUserValid(std::string username);
	bool doesPasswordMatch(std::string username, std::string password);
	void addNewUser(std::string username, std::string password, std::string email, std::string address, std::string phoneNumber, std::string birthDate);
	bool createTable(std::string sqlStatement);
	void addNewQuestion(std::string jsonQuestion);
	void updateTotalTimeForAnswer(std::string username, float totalTimeForAnswer);
	void updateUserAvgAnswerTime(std::string username, float playerAverageAnswerTime);
	void increaseUserNumCorrectAns(std::string username);
	void increaseUserNumOfGames(std::string username);
	void increaseUserNumTotalAnswers(std::string username);
	float getPlayerAverageAnswerTime(std::string username);
	int getNumOfCorrectAnswers(std::string username);
	int getNumOfTotalAnswers(std::string username);
	int getNumOfPlayerGames(std::string username);
	float getTotalTimeForAnswer(std::string username);
	double getSpecificStatistic(std::string username, std::string statisticName);
	std::vector<Question> getAllQuestions();
	static int callbackGetUsers(void* data, int argc, char** argv, char** azColName);
	static int callbackGetStatistics(void* data, int argc, char** argv, char** azColName);
	static int callbackGetQuestions(void* data, int argc, char** argv, char** azColName);
	std::list<std::string> getUsernames();

	bool open();
	void close();

private:
	sqlite3* _db;

};

