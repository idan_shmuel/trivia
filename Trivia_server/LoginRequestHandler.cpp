#include "LoginRequestHandler.h"

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* handlerFactory) : IRequestHandler()
{
	this->m_handlerFactory = handlerFactory;
}

LoginRequestHandler::~LoginRequestHandler()
{
	delete this->m_handlerFactory;
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.messageCode == LOGIN_CODE || requestInfo.messageCode == SIGNUP_CODE);
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestResult;
	ErrorResponse errorResponse;
	unsigned char* fullMessage = {};

	try
	{
		switch (requestInfo.messageCode)
		{
		case LOGIN_CODE:
			return handleLoginRequest(requestInfo);

		case SIGNUP_CODE:
			return handleSignupRequest(requestInfo);

		default:
			break;
		}
	}
	catch (std::exception e)
	{
		std::cout << "Error: " << e.what() << std::endl;
		requestResult.newHandler = nullptr;
		errorResponse.message = "ERROR";
		fullMessage = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.buffer = fullMessage;
		return requestResult;
	}
}

RequestResult LoginRequestHandler::handleLoginRequest(RequestInfo requestInfo)
{
	LoginResponse loginResponse;
	RequestResult requestResult;
	LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(requestInfo.buffer);
	loginResponse.status = this->m_handlerFactory->getLoginManager()->login(loginRequest.username, loginRequest.password);

	//the login succeed.
	if (loginResponse.status == 1)
	{
		LoggedUser* loggedUser = new LoggedUser(loginRequest.username);
		requestResult.newHandler = this->m_handlerFactory->createMenuRequestHandler(loggedUser, this->m_handlerFactory);
	}
	else
	{	
		requestResult.newHandler = NULL;
	}
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(loginResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult LoginRequestHandler::handleSignupRequest(RequestInfo requestInfo)
{
	SignupResponse signupResponse;
	RequestResult requestResult;
	SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(requestInfo.buffer);
	signupResponse.status = this->m_handlerFactory->getLoginManager()->signup(signupRequest.username, signupRequest.password, signupRequest.email, signupRequest.address, signupRequest.phoneNumber, signupRequest.birthDate);
	requestResult.newHandler = NULL;
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(signupResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}
