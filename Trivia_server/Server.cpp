#include "Server.h"

Server::Server()
{
}

void Server::run()
{
	std::thread t_connector(&Communicator::startHandleRequests, this->m_communicator);
	t_connector.detach();
	std::string userConsoleInput;
	while (userConsoleInput != "EXIT")
	{
		std::cin >> userConsoleInput;
	}
}
