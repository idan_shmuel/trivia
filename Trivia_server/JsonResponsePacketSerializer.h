#pragma once
#include "pch.h"
#include "ResponseStructs.h"

class JsonResponsePacketSerializer
{
public:
	JsonResponsePacketSerializer();
	static unsigned char* serializeResponse(ErrorResponse errorResponse);
	static unsigned char* serializeResponse(LoginResponse loginResponse);
	static unsigned char* serializeResponse(SignupResponse signupResponse);
	static unsigned char* serializeResponse(LogoutResponse logoutResponse);
	static unsigned char* serializeResponse(GetRoomsResponse getRoomsResponse);
	static unsigned char* serializeResponse(GetPlayersInRoomResponse getPlayersInRoomResponse);
	static unsigned char* serializeResponse(JoinRoomResponse joinRoomResponse);
	static unsigned char* serializeResponse(CreateRoomResponse createRoomResponse);
	static unsigned char* serializeResponse(TopFiveResponse highScoreResponse);
	static unsigned char* serializeResponse(CloseRoomResponse closeRoomResponse);
	static unsigned char* serializeResponse(StartGameResponse startGameResponse);
	static unsigned char* serializeResponse(GetRoomStateResponse getRoomStateResponse);
	static unsigned char* serializeResponse(LeaveRoomResponse leaveRoomResponse);
	static unsigned char* serializeResponse(GetGameResultsResponse getGameResultsResponse);
	static unsigned char* serializeResponse(SubmitAnswerResponse submitAnswerResponse);
	static unsigned char* serializeResponse(GetQuestionResponse getQuestionResponse);
	static unsigned char* serializeResponse(LeaveGameResponse leaveGameResponse);
	static unsigned char* serializeResponse(PlayerStatisticsResponse playerStatisticsResponse);

	//The function receive json and code message. It returns full message by our protocol in bytes.
	static unsigned char* jsonToBytesMessage(nlohmann::json json, std::string codeMessage);
};

