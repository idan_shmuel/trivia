#include "pch.h"
#include "Server.h"
#include "WSAInitializer.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "ResponseStructs.h"
#include "requestStructs.h"

int main(void)
{
	try
	{
		WSAInitializer wsaInit;
		Server server = Server();
		server.run();
	}
	catch (std::exception & e)
	{
		std::cout << "Error: " << e.what() << std::endl;
	}

	return 0;
}