#include "JsonRequestPacketDeserializer.h"

JsonRequestPacketDeserializer::JsonRequestPacketDeserializer()
{
}

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(unsigned char* buffer)
{
	LoginRequest loginRequest;

	std::string message(reinterpret_cast<char*>(buffer));
	nlohmann::json jsonMessage = nlohmann::json::parse(message.begin(), message.end());
	
	loginRequest.username = jsonMessage["username"];
	loginRequest.password = jsonMessage["password"];

	return loginRequest;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(unsigned char* buffer)
{
	SignupRequest signupRequest;

	std::string message(reinterpret_cast<char*>(buffer));
	nlohmann::json jsonMessage = nlohmann::json::parse(message.begin(), message.end());

	signupRequest.username = jsonMessage["username"];
	signupRequest.password = jsonMessage["password"];
	signupRequest.email = jsonMessage["email"];
	signupRequest.address = jsonMessage["address"];
	signupRequest.birthDate = jsonMessage["birthDate"];
	signupRequest.phoneNumber = jsonMessage["phoneNumber"];

	return signupRequest;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(unsigned char* buffer)
{
	GetPlayersInRoomRequest getPlayersInRoomRequest;

	std::string message(reinterpret_cast<char*>(buffer));
	nlohmann::json jsonMessage = nlohmann::json::parse(message.begin(), message.end());

	getPlayersInRoomRequest.roomId = jsonMessage["roomId"];

	return getPlayersInRoomRequest;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(unsigned char* buffer)
{
	JoinRoomRequest joinRoomRequest;

	std::string message(reinterpret_cast<char*>(buffer));
	nlohmann::json jsonMessage = nlohmann::json::parse(message.begin(), message.end());

	joinRoomRequest.roomId = jsonMessage["roomId"];

	return joinRoomRequest;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(unsigned char* buffer)
{
	CreateRoomRequest createRoomRequest;

	std::string message(reinterpret_cast<char*>(buffer));
	nlohmann::json jsonMessage = nlohmann::json::parse(message.begin(), message.end());

	createRoomRequest.answerTimeout = jsonMessage["answerTimeout"];
	createRoomRequest.maxUsers = jsonMessage["maxPlayers"];
	createRoomRequest.questionCount = jsonMessage["questionCount"];
	createRoomRequest.roomName = jsonMessage["roomName"];

	return createRoomRequest;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(unsigned char* buffer)
{
	SubmitAnswerRequest submitAnswerRequest;

	std::string message(reinterpret_cast<char*>(buffer));
	nlohmann::json jsonMessage = nlohmann::json::parse(message.begin(), message.end());

	submitAnswerRequest.answerSelected = jsonMessage["answerSelected"];

	return submitAnswerRequest;
}
