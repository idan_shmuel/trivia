#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "MenuRequestHandler.h"

class LoginRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* m_handlerFactory;
	RequestResult handleLoginRequest(RequestInfo requestInfo);
	RequestResult handleSignupRequest(RequestInfo requestInfo);

public:
	LoginRequestHandler(RequestHandlerFactory* hndlerFactory);
	~LoginRequestHandler();
	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo);
};

