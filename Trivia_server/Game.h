#pragma once
#include "pch.h"
#include "LoggedUser.h"
#include "GameStructs.h"
#include "Question.h"

class Game
{
private:
	std::vector<Question> m_questions;
	std::map<LoggedUser*, GameData> m_players;
	int m_roomID;

public:
	Game(Game &game);
	Game(std::vector<Question> m_questions, std::map<LoggedUser*, GameData> m_players, int roomId);
	Question getQuestionForUser(LoggedUser* user);
	bool isPlayerInGame(LoggedUser* user);
	std::pair <bool, int> submitAnswer(std::string answerSelected, LoggedUser* user);
	void removePlayer(LoggedUser* user);
	std::map<LoggedUser*, GameData> getPlayers();
	int getRoomID();
};

