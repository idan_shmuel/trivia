#include "RoomMemberRequestHandler.h"


RoomMemberRequestHandler::RoomMemberRequestHandler(Room* room, LoggedUser* user, RoomManager* roomManager, RequestHandlerFactory* handlerFactory) : IRequestHandler()
{
	this->m_handlerFactory = handlerFactory;
	this->m_room = room;
	this->m_roomManager = roomManager;
	this->m_user = user;
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.messageCode == LEAVE_ROOM_CODE || requestInfo.messageCode == GET_ROOM_STATE_CODE);
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestResult;
	ErrorResponse errorResponse;
	unsigned char* fullMessage = {};

	try
	{
		switch (requestInfo.messageCode)
		{
		case LEAVE_ROOM_CODE:
			return handleLeaveRoom(requestInfo);

		case GET_ROOM_STATE_CODE:
			return handleGetRoomState(requestInfo);

		default:
			break;
		}
	}
	catch (std::exception e)
	{
		std::cout << "Error: " << e.what() << std::endl;
		requestResult.newHandler = nullptr;
		errorResponse.message = "ERROR";
		fullMessage = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.buffer = fullMessage;
		return requestResult;
	}
}

RequestResult RoomMemberRequestHandler::handleLeaveRoom(RequestInfo requestInfo)
{
	LeaveRoomResponse leaveRoomResponse;
	RequestResult requestResult;
	leaveRoomResponse.status = SUCCESS_CODE;
	requestResult.newHandler = this->m_handlerFactory->createMenuRequestHandler(this->m_user, this->m_handlerFactory);
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(leaveRoomResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult RoomMemberRequestHandler::handleGetRoomState(RequestInfo requestInfo)
{
	GetRoomStateResponse* getRoomStateResponse = this->m_roomManager->getRoomState(this->m_room->getRoomData()->id);
	RequestResult requestResult;
	if (getRoomStateResponse->hasGameBegun)
	{
		requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(this->m_handlerFactory, this->m_user, this->m_handlerFactory->getGameManager()->getCurrentGame(this->m_user));
	}
	else
	{
		requestResult.newHandler = NULL;
	}
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(*getRoomStateResponse);
	requestResult.buffer = fullMessage;
	delete getRoomStateResponse;
	return requestResult;
}