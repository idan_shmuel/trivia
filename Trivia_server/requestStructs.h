#pragma once
#include "pch.h"
#include "IRequestHandler.h"

class IRequestHandler;
typedef struct LoginRequest
{
	std::string username;
	std::string password;	
}LoginRequest;

typedef struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
	std::string address;
	std::string phoneNumber;
	std::string birthDate;
}SignupRequest;

typedef struct RequestInfo
{
	int messageCode;
	std::time_t receivalTime;
	unsigned char* buffer;
}RequestInfo;


typedef struct RequestResult
{
	unsigned char* buffer;
	IRequestHandler* newHandler;
}RequestResult;

typedef struct GetPlayersInRoomRequest
{
	unsigned int roomId;
}GetPlayersInRoomRequest;

typedef struct JoinRoomRequest
{
	unsigned int roomId;
}JoinRoomRequest;

typedef struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
}CreateRoomRequest;

typedef struct SubmitAnswerRequest
{
	std::string answerSelected;
}SubmitAnswerRequest;