#include "Communicator.h"

Communicator::Communicator()
{
	this->handlerFactory = new RequestHandlerFactory();
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

void Communicator::startHandleRequests()
{
	bindAndListen();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}

void Communicator::bindAndListen()
{
	struct sockaddr_in socketAddress = { 0 };

	socketAddress.sin_port = htons(PORT); // port that server will listen for
	socketAddress.sin_family = AF_INET;   // must be AF_INET
	socketAddress.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->m_serverSocket, (struct sockaddr*) & socketAddress, sizeof(socketAddress)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(this->m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;

}

void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{
		RequestResult requestResult;
		RequestInfo requestInfo;
		//added 1 for NULL terminator.
		unsigned char codeAndLength[5 * BYTE + 1];
		int messageCode, messageLen;
		std::map<SOCKET, IRequestHandler*>::iterator it;
		while (true)
		{
			getDataFromClient(clientSocket, 5 * BYTE, codeAndLength);
			messageCode = getMessageCode(codeAndLength);
			requestInfo.messageCode = messageCode;
			requestInfo.receivalTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
			messageLen = getMessageLength(codeAndLength);
			//added 1 for NULL terminator.
			requestInfo.buffer = new unsigned char[messageLen + 1];
			if (messageLen != 0)
			{
				getDataFromClient(clientSocket, messageLen, requestInfo.buffer);
			}
			for (it = this->m_clients.begin(); it != this->m_clients.end(); ++it)
			{
				if (it->first == clientSocket)
				{
					if (it->second->isRequestRelevant(requestInfo))
					{
						requestResult = it->second->handleRequest(requestInfo);
						sendDataToClient(clientSocket, std::string((char*)requestResult.buffer));
						if (requestResult.newHandler != NULL)
						{
							delete it->second;
							it->second = requestResult.newHandler;
							delete[] requestResult.buffer;
						}
						
					}
				}
			}
			delete[] requestInfo.buffer;

		}
	}
	catch (std::exception & e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
}

void Communicator::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(this->m_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	
	LoginRequestHandler* clientHandler = new LoginRequestHandler(this->handlerFactory);
	this->m_clients.insert(std::make_pair(client_socket, clientHandler));

	// the function that handle the conversation with the client
	std::thread clientThread(&Communicator::handleNewClient, this, client_socket);
	clientThread.detach();
}

void Communicator::sendDataToClient(SOCKET clientSocket, std::string serverMessage)
{
	const char* data = serverMessage.c_str();

	if (send(clientSocket, data, serverMessage.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

void Communicator::getDataFromClient(SOCKET clientSocket, int bytesNum, unsigned char* buffer)
{
	
	int res = recv(clientSocket, (char*)buffer, bytesNum, 0);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(clientSocket);
		throw std::exception(s.c_str());
	}

	buffer[bytesNum] = 0;
}

int Communicator::getMessageCode(unsigned char* clientMessage)
{
	//convertion: unsigned char* -> char*
	reinterpret_cast<char*>(clientMessage);
	
	std::string messageCodeBinary;

	for (int i = 0; i < BYTE; i++)
	{
		messageCodeBinary += clientMessage[i];
	}

	return this->binaryToDecimal(messageCodeBinary);
}

int Communicator::getMessageLength(unsigned char* clientMessage)
{
	//convertion: unsigned char* -> char*
	reinterpret_cast<char*>(clientMessage);

	std::string messageLengthBinary;

	for (int i = BYTE; i < 5 * BYTE; i++)
	{
		messageLengthBinary += clientMessage[i];
	}
	return this->binaryToDecimal(messageLengthBinary);
}

std::string Communicator::getMessageContentInBits(unsigned char* clientMessage, int messageLength)
{
	//convertion: unsigned char* -> char*
	reinterpret_cast<char*>(clientMessage);

	std::string binaryMessage;
	for (int i = 0; i < BYTE * messageLength; i++)
	{
		binaryMessage += clientMessage[i];
	}
	return binaryMessage;
}

std::string Communicator::getJsonMessage(std::string bitsContent)
{
	std::string jsonMessage;
	std::stringstream sstream(bitsContent);
	while (sstream.good())
	{
		std::bitset<8> bits;
		sstream >> bits;
		char c = char(bits.to_ulong());
		jsonMessage += c;
	}
	return jsonMessage;
}

int Communicator::binaryToDecimal(std::string binaryMessage)
{
	int binaryNumber = stoi(binaryMessage);
	int decimalNumber = 0, i = 0, remainder;
	while (binaryNumber != 0)
	{
		remainder = binaryNumber % 10;
		binaryNumber /= 10;
		decimalNumber += remainder * pow(2, i);
		++i;
	}

	return decimalNumber;
}

