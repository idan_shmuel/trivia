#pragma once
#include "pch.h"

class Question
{
private:
	std::string m_question;
	std::vector<std::string> m_possibleAnswers;

public:
	Question();
	Question(std::string question, std::vector<std::string> possibleAnswers);
	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	std::string getCorrectAnswer();
};

