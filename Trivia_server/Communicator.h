#pragma once
#include "pch.h"
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#define PORT 5353

class Communicator
{ 
public:
	Communicator();
	void startHandleRequests();

private:
	RequestHandlerFactory* handlerFactory;
	std::map<SOCKET, IRequestHandler*> m_clients;
	SOCKET m_serverSocket;
	void bindAndListen();
	void handleNewClient(SOCKET clientSocket);
	void accept();
	void sendDataToClient(SOCKET clientSocket, std::string serverMessage);
	void getDataFromClient(SOCKET clientSocket, int bytesNum, unsigned char* buffer);
	int getMessageCode(unsigned char* clientMessage);
	int getMessageLength(unsigned char* clientMessage);
	std::string getMessageContentInBits(unsigned char* clientMessage, int messageLength);
	std::string getJsonMessage(std::string bitsContent);
	int binaryToDecimal(std::string binaryMessage);

};