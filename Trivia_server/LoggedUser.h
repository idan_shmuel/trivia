#pragma once
#include "pch.h"

class LoggedUser
{
private:
	std::string m_username;

public:
	LoggedUser(std::string username);
	std::string getUsername();
};

