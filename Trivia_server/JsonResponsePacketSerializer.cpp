#include "JsonResponsePacketSerializer.h"

JsonResponsePacketSerializer::JsonResponsePacketSerializer()
{
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(ErrorResponse errorResponse)
{
	// conversion: errorResponse -> json
	nlohmann::json errorResponseJson;
	errorResponseJson["message"] = errorResponse.message;

	// Error code is 0.
	std::string codeMessage = "00000000";

	unsigned char* fullMessageChars = jsonToBytesMessage(errorResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(LoginResponse loginResponse)
{
	// conversion: loginResponse -> json
	nlohmann::json loginResponseJson;
	loginResponseJson["status"] = loginResponse.status;

	// Login code is 1.
	std::string codeMessage = "00000001";

	unsigned char* fullMessageChars = jsonToBytesMessage(loginResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(SignupResponse signupResponse)
{
	// conversion: signupResponse -> json
	nlohmann::json signupResponseJson;
	signupResponseJson["status"] = signupResponse.status;

	// Signup code is 2.
	std::string codeMessage = "00000010";

	unsigned char* fullMessageChars = jsonToBytesMessage(signupResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(LogoutResponse logoutResponse)
{
	// conversion: logoutResponse -> json
	nlohmann::json logoutResponseJson;
	logoutResponseJson["status"] = logoutResponse.status;

	// Logout code is 3.
	std::string codeMessage = "00000011";

	unsigned char* fullMessageChars = jsonToBytesMessage(logoutResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse getRoomsResponse)
{
	// conversion: getRoomsResponse -> json
	nlohmann::json getRoomsResponseJson;
	getRoomsResponseJson["status"] = getRoomsResponse.status;
	for (int i = 0; i < getRoomsResponse.rooms.size(); i++)
	{
		getRoomsResponseJson["rooms"][i]["id"] = getRoomsResponse.rooms[i].id;
		getRoomsResponseJson["rooms"][i]["timePerQuestion"] = getRoomsResponse.rooms[i].timePerQuestion;
		getRoomsResponseJson["rooms"][i]["isActive"] = getRoomsResponse.rooms[i].isActive;
		getRoomsResponseJson["rooms"][i]["maxPlayers"] = getRoomsResponse.rooms[i].maxPlayers;
		getRoomsResponseJson["rooms"][i]["name"] = getRoomsResponse.rooms[i].name;
		getRoomsResponseJson["rooms"][i]["questionCount"] = getRoomsResponse.rooms[i].questionCount;
	}

	// Get_rooms code is 4.
	std::string codeMessage = "00000100";

	unsigned char* fullMessageChars = jsonToBytesMessage(getRoomsResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse getPlayersInRoomResponse)
{
	// conversion: getPlayersInRoomResponse -> json
	nlohmann::json getPlayersInRoomResponseJson;
	getPlayersInRoomResponseJson["status"] = getPlayersInRoomResponse.status;
	getPlayersInRoomResponseJson["rooms"] = getPlayersInRoomResponse.rooms;
	

	// Get_players_in_room code is 5.
	std::string codeMessage = "00000101";

	unsigned char* fullMessageChars = jsonToBytesMessage(getPlayersInRoomResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse joinRoomResponse)
{
	// conversion: joinRoomResponse -> json
	nlohmann::json joinRoomResponseJson;
	joinRoomResponseJson["status"] = joinRoomResponse.status;

	// Join_Room code is 6.
	std::string codeMessage = "00000110";

	unsigned char* fullMessageChars = jsonToBytesMessage(joinRoomResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse createRoomResponse)
{
	// conversion: createRoomResponse -> json
	nlohmann::json createRoomResponseJson;
	createRoomResponseJson["status"] = createRoomResponse.status;

	// Create_room code is 7.
	std::string codeMessage = "00000111";

	unsigned char* fullMessageChars = jsonToBytesMessage(createRoomResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(TopFiveResponse topFiveResponse)
{
	// conversion: highScoreResponse -> json
	nlohmann::json highScoreResponseJson;
	highScoreResponseJson["status"] = topFiveResponse.status;
	highScoreResponseJson["highScores"] = topFiveResponse.topFive;

	// High_score code is 8.
	std::string codeMessage = "00001000";

	unsigned char* fullMessageChars = jsonToBytesMessage(highScoreResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse closeRoomResponse)
{
	// conversion: closeRoomResponse -> json
	nlohmann::json closeRoomResponseJson;
	closeRoomResponseJson["status"] = closeRoomResponse.status;

	// Close_room code is 9.
	std::string codeMessage = "00001001";

	unsigned char* fullMessageChars = jsonToBytesMessage(closeRoomResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(StartGameResponse startGameResponse)
{
	// conversion: startGameResponse -> json
	nlohmann::json startGameResponseJson;
	startGameResponseJson["status"] = startGameResponse.status;

	// Start_game code is 10.
	std::string codeMessage = "00001010";

	unsigned char* fullMessageChars = jsonToBytesMessage(startGameResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse getRoomStateResponse)
{
	// conversion: getRoomStateResponse -> json
	nlohmann::json getRoomStateResponseJson;
	getRoomStateResponseJson["status"] = getRoomStateResponse.status;
	getRoomStateResponseJson["hasGameBegun"] = getRoomStateResponse.hasGameBegun;
	getRoomStateResponseJson["players"] = getRoomStateResponse.players;
	getRoomStateResponseJson["questionCount"] = getRoomStateResponse.questionCount;
	getRoomStateResponseJson["answerTimeout"] = getRoomStateResponse.answerTimeout;

	// Room_state code is 11.
	std::string codeMessage = "00001011";

	unsigned char* fullMessageChars = jsonToBytesMessage(getRoomStateResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse leaveRoomResponse)
{
	// conversion: leaveRoomResponse -> json
	nlohmann::json leaveRoomResponseJson;
	leaveRoomResponseJson["status"] = leaveRoomResponse.status;

	// Leave_room code is 12.
	std::string codeMessage = "00001100";

	unsigned char* fullMessageChars = jsonToBytesMessage(leaveRoomResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse getGameResultsResponse)
{
	// conversion: getGameResultsResponse -> json
	nlohmann::json getGameResultsResponseJson;
	getGameResultsResponseJson["status"] = getGameResultsResponse.status;
	for (int i = 0; i < getGameResultsResponse.results.size(); i++)
	{
		getGameResultsResponseJson["results"][i]["username"] = getGameResultsResponse.results[i].username;
		getGameResultsResponseJson["results"][i]["numOfTotalAnswers"] = getGameResultsResponse.results[i].totalAnswerCount;
		getGameResultsResponseJson["results"][i]["playerAverageAnswerTime"] = getGameResultsResponse.results[i].averageAnswerTime;
		getGameResultsResponseJson["results"][i]["numOfCorrectAnswers"] = getGameResultsResponse.results[i].correctAnswerCount;
		getGameResultsResponseJson["results"][i]["numOfPlayerGames"] = getGameResultsResponse.results[i].numOfPlayerGames;
	}

	// Game_results code is 13.
	std::string codeMessage = "00001101";

	unsigned char* fullMessageChars = jsonToBytesMessage(getGameResultsResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse submitAnswerResponse)
{
	// conversion: submitAnswerResponse -> json
	nlohmann::json submitAnswerResponseJson;
	submitAnswerResponseJson["status"] = submitAnswerResponse.status;
	submitAnswerResponseJson["correctAnswerId"] = submitAnswerResponse.correctAnswerId;

	// Submit_answer code is 14.
	std::string codeMessage = "00001110";

	unsigned char* fullMessageChars = jsonToBytesMessage(submitAnswerResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse getQuestionResponse)
{
	// conversion: getQuestionResponse -> json
	nlohmann::json getQuestionResponseJson;
	getQuestionResponseJson["status"] = getQuestionResponse.status;
	getQuestionResponseJson["question"] = getQuestionResponse.question;
	getQuestionResponseJson["answers"] = getQuestionResponse.answers;

	// Get_question code is 15.
	std::string codeMessage = "00001111";

	unsigned char* fullMessageChars = jsonToBytesMessage(getQuestionResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse leaveGameResponse)
{
	// conversion: leaveGameResponse -> json
	nlohmann::json leaveGameResponseJson;
	leaveGameResponseJson["status"] = leaveGameResponse.status;

	// Leave_game code is 16.
	std::string codeMessage = "00010000";

	unsigned char* fullMessageChars = jsonToBytesMessage(leaveGameResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::serializeResponse(PlayerStatisticsResponse playerStatisticsResponse)
{
	// conversion: playerStatisticsResponse -> json
	nlohmann::json playerStatisticsResponseJson;
	playerStatisticsResponseJson["status"] = playerStatisticsResponse.status;
	playerStatisticsResponseJson["username"] = playerStatisticsResponse.username;
	playerStatisticsResponseJson["numOfPlayerGames"] = playerStatisticsResponse.numOfPlayerGames;
	playerStatisticsResponseJson["correctAnswerCount"] = playerStatisticsResponse.correctAnswerCount;
	playerStatisticsResponseJson["totalAnswerCount"] = playerStatisticsResponse.totalAnswerCount;
	playerStatisticsResponseJson["averageAnswerTime"] = playerStatisticsResponse.averageAnswerTime;

	// Player_statistics code is 17.
	std::string codeMessage = "00010001";

	unsigned char* fullMessageChars = jsonToBytesMessage(playerStatisticsResponseJson, codeMessage);

	return fullMessageChars;
}

unsigned char* JsonResponsePacketSerializer::jsonToBytesMessage(nlohmann::json json, std::string codeMessage)
{
	// Convert json to string.
	std::string responseJsonString = json.dump();
	std::cout << responseJsonString;

	std::string dataLength = std::string(5 * 8 - std::to_string(responseJsonString.size()).size() * 8, '0').append(std::bitset<8>(responseJsonString.size()).to_string());

	std::string data = "";
	for (int i = 0; i < responseJsonString.size(); i++)
	{
		data += std::bitset<8>(responseJsonString[i]).to_string();
	}

	// Full message by our protocol.
	std::string fullMessage = codeMessage + dataLength + data;

	// Casting full message string to unsigned char* buffer.
	//added 1 for NULL terminator.
	unsigned char* fullMessageChars = new unsigned char[fullMessage.size() + 1];
	std::copy(fullMessage.begin(), fullMessage.end(), fullMessageChars);
	fullMessageChars[fullMessage.length()] = 0;

	return fullMessageChars;
}
