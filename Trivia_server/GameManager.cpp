#include "GameManager.h"

GameManager::GameManager()
{
	this->m_database = new SqliteDataBase();
}

GameManager::~GameManager()
{
	delete this->m_database;
}

Game* GameManager::createGame(Room* room)
{
	std::vector<LoggedUser*> users = room->getAllUsers();
	std::vector<Question> questions = this->m_database->getAllQuestions();
	GameData gameData = GameData(questions[0], 0, 0, 0, 0);
	std::map<LoggedUser*, GameData> players;
	for (int i = 0; i < users.size(); i++)
	{
		players[users[i]] = gameData;
	}
	this->m_games.push_back(new Game(questions, players, room->getRoomData()->id));
	return this->m_games.back();
}

void GameManager::deleteGame(Game* game)
{
	for (int i = 0; i < this->m_games.size(); i++)
	{
		if (this->m_games[i] == game)
		{
			this->m_games.erase(this->m_games.begin() + i);
			delete game;
		}
	}
}

Game* GameManager::getCurrentGame(LoggedUser* user)
{
	for (int i = 0; i < this->m_games.size(); i++)
	{
		if (this->m_games[i]->isPlayerInGame(user))
		{
			return this->m_games[i];
		}
	}
}
