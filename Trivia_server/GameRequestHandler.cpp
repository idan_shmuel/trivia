#include "GameRequestHandler.h"

RequestResult GameRequestHandler::handleLeaveGameRequest(RequestInfo requestInfo)
{
	LeaveRoomResponse leaveRoomResponse;
	RequestResult requestResult;
	this->m_game->removePlayer(this->m_loggedUser);
	if (this->m_game->getPlayers().empty())
	{
		this->m_handlerFactory->getRoomManager()->deleteRoom(this->m_game->getRoomID());
		this->m_handlerFactory->getGameManager()->deleteGame(this->m_game);
	}
	leaveRoomResponse.status = SUCCESS_CODE;
	requestResult.newHandler = this->m_handlerFactory->createMenuRequestHandler(this->m_loggedUser, this->m_handlerFactory);
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(leaveRoomResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult GameRequestHandler::handleGetQuestionRequest(RequestInfo requestInfo)
{
	GetQuestionResponse getQuestionResponse;
	RequestResult requestResult;
	Question question = this->m_game->getQuestionForUser(this->m_loggedUser);
	//this array will be used for check where the answers placed.
	//-1 it means that the place is empty
	int alreadyWritten[4] = {-1, -1, -1, -1};
	getQuestionResponse.question = question.getQuestion();
	setAnswersForQuestion(alreadyWritten, question, getQuestionResponse);

	if (getQuestionResponse.question != "")
	{
		getQuestionResponse.status = SUCCESS_CODE;
	}
	else
	{
		getQuestionResponse.status = ERROR_CODE;
	}
	this->m_tStart = clock();
	requestResult.newHandler = NULL;
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(getQuestionResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult GameRequestHandler::handleSubmitAnswerRequest(RequestInfo requestInfo)
{
	SubmitAnswerResponse submitAnswerResponse;
	RequestResult requestResult;
	SubmitAnswerRequest submitAnswerRequest = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(requestInfo.buffer);
	//I divide and multiply in 100, because its takes only 2 digits after the point.
	this->m_totalTimeForAnswer = roundf(((clock() - this->m_tStart) / (double)CLOCKS_PER_SEC) * 100) / 100;
	this->m_handlerFactory->getStatisticsManager()->increaseUserNumTotalAnswers(this->m_loggedUser->getUsername());
	this->m_handlerFactory->getStatisticsManager()->updateUserAvgAnswerTime(this->m_loggedUser->getUsername(), this->m_totalTimeForAnswer);
	std::pair<bool, int> submitAns = this->m_handlerFactory->getGameManager()->getCurrentGame(this->m_loggedUser)->submitAnswer(submitAnswerRequest.answerSelected, this->m_loggedUser);
	if (submitAns.first)
	{
		submitAnswerResponse.status = SUCCESS_CODE;
		this->m_handlerFactory->getStatisticsManager()->increaseUserNumCorrectAns(this->m_loggedUser->getUsername());
	}
	else
	{
		submitAnswerResponse.status = ERROR_CODE;
	}
	submitAnswerResponse.correctAnswerId = submitAns.second;
	requestResult.newHandler = NULL;
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(submitAnswerResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult GameRequestHandler::handleGetGameResult(RequestInfo requestInfo)
{
	return RequestResult();
}

void GameRequestHandler::setAnswersForQuestion(int* alreadyWritten, Question question, GetQuestionResponse& getQuestionResponse)
{
	int currAnswer = 0;

	// i will generate number and place the correct answer there and after that i place the other answers.
	srand(time(NULL));

	//generate num between 0 to 3
	currAnswer = rand() % 4;
	alreadyWritten[currAnswer] = currAnswer;
	getQuestionResponse.answers.insert(std::make_pair(currAnswer + 1, question.getCorrectAnswer()));
	for (int i = 2; i < 5; i++)
	{
		currAnswer = rand() % 4;
		while (alreadyWritten[currAnswer] != -1)
		{
			currAnswer = rand() % 4;
		}
		alreadyWritten[currAnswer] = currAnswer;
		getQuestionResponse.answers.insert(std::make_pair(currAnswer + 1, question.getPossibleAnswers()[i - 1]));
	}
}

GameRequestHandler::GameRequestHandler(RequestHandlerFactory* handleFactory, LoggedUser* loggedUser,Game* game) : IRequestHandler()
{
	this->m_handlerFactory = handleFactory;
	this->m_loggedUser = loggedUser;
	this->m_game = game;
}

bool GameRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.messageCode == GET_GAME_RESULTS_CODE || requestInfo.messageCode == SUBMIT_ANSWER_CODE || requestInfo.messageCode == GET_QUESTION_CODE || requestInfo.messageCode == LEAVE_GAME_CODE);
}

RequestResult GameRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestResult;
	ErrorResponse errorResponse;
	unsigned char* fullMessage = {};
	try
	{
		switch (requestInfo.messageCode)
		{
		case GET_GAME_RESULTS_CODE:
			return handleGetGameResult(requestInfo);

		case SUBMIT_ANSWER_CODE:
			return handleSubmitAnswerRequest(requestInfo);

		case GET_QUESTION_CODE:
			return handleGetQuestionRequest(requestInfo);

		case LEAVE_GAME_CODE:
			return handleLeaveGameRequest(requestInfo);

		default:
			break;
		}
	}
	catch (std::exception e)
	{
		std::cout << "Error: " << e.what() << std::endl;
		requestResult.newHandler = nullptr;
		errorResponse.message = "ERROR";
		fullMessage = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.buffer = fullMessage;
		return requestResult;
	}
}
