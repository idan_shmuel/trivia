#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(Room* room, LoggedUser* user, RoomManager* roomManager, RequestHandlerFactory* handlerFactory) : IRequestHandler()
{
	this->m_room = room;
	this->m_user = user;
	this->m_roomManager = roomManager;
	this->m_handlerFactory = handlerFactory;
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.messageCode == CLOSE_ROOM_CODE || requestInfo.messageCode == START_GAME_CODE || requestInfo.messageCode == GET_ROOM_STATE_CODE);
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult requestResult;
	ErrorResponse errorResponse;
	unsigned char* fullMessage = {};

	try
	{
		switch (requestInfo.messageCode)
		{
		case CLOSE_ROOM_CODE:
			return handleCloseRoom(requestInfo);

		case START_GAME_CODE:
			return handleStartGame(requestInfo);

		case GET_ROOM_STATE_CODE:
			return handleGetRoomState(requestInfo);

		default:
			break;
		}
	}
	catch (std::exception e)
	{
		std::cout << "Error: " << e.what() << std::endl;
		requestResult.newHandler = nullptr;
		errorResponse.message = "ERROR";
		fullMessage = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.buffer = fullMessage;
		return requestResult;
	}
}

RequestResult RoomAdminRequestHandler::handleCloseRoom(RequestInfo requestInfo)
{
	CloseRoomResponse closeRoomResponse;
	RequestResult requestResult;
	bool isSuccess;
	isSuccess = this->m_roomManager->deleteRoom(this->m_room->getRoomData()->id);
	if (isSuccess)
	{
		closeRoomResponse.status = SUCCESS_CODE;
		requestResult.newHandler = this->m_handlerFactory->createMenuRequestHandler(this->m_user, this->m_handlerFactory);
	}
	else
	{
		closeRoomResponse.status = ERROR_CODE;
	}
	
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(closeRoomResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult RoomAdminRequestHandler::handleStartGame(RequestInfo requestInfo)
{
	StartGameResponse startGameResponse;
	RequestResult requestResult;
	startGameResponse.status = SUCCESS_CODE;
	this->m_room->startsGame();
	for (int i = 0; i < this->m_room->getAllUsers().size();i++)
	{
		this->m_handlerFactory->getStatisticsManager()->increaseUserNumOfGames(this->m_room->getAllUsers()[i]->getUsername());
	}
	requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(this->m_handlerFactory, this->m_user, this->m_handlerFactory->getGameManager()->createGame(this->m_room));
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(startGameResponse);
	requestResult.buffer = fullMessage;
	return requestResult;
}

RequestResult RoomAdminRequestHandler::handleGetRoomState(RequestInfo requestInfo)
{
	GetRoomStateResponse* getRoomStateResponse = this->m_roomManager->getRoomState(this->m_room->getRoomData()->id);
	RequestResult requestResult;
	requestResult.newHandler = NULL;
	unsigned char* fullMessage = JsonResponsePacketSerializer::serializeResponse(*getRoomStateResponse);
	requestResult.buffer = fullMessage;
	delete getRoomStateResponse;
	return requestResult;
}
