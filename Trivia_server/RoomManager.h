#pragma once
#include "Room.h"
#include "ResponseStructs.h"

class RoomManager
{
private:
	std::vector<Room*> m_rooms;
	int LatestRoomID;

public:
	RoomManager();
	void createRoom(LoggedUser* roomCreator, RoomData* roomData);
	bool deleteRoom(int ID);
	GetRoomStateResponse* getRoomState(int ID);
	std::vector<Room*> getRooms();
};

