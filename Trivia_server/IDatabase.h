#pragma once
#include "pch.h"
#include "sqlite3.h"

class IDatabase
{
public:
	IDatabase();

	virtual bool doesUserExist(std::string username) = 0;
	virtual bool doesUserValid(std::string username) = 0;
	virtual bool doesPasswordMatch(std::string username, std::string password) = 0;
	virtual void addNewUser(std::string username, std::string password, std::string email, std::string address, std::string phoneNumber, std::string birthDate) = 0;
	virtual bool createTable(std::string sqlStatement) = 0;
	virtual void addNewQuestion(std::string jsonQuestion) = 0;
	virtual void updateTotalTimeForAnswer(std::string username, float totalTimeForAnswer) = 0;
	virtual void updateUserAvgAnswerTime(std::string username, float playerAverageAnswerTime) = 0;
	virtual void increaseUserNumCorrectAns(std::string username) = 0;
	virtual void increaseUserNumOfGames(std::string username) = 0;
	virtual void increaseUserNumTotalAnswers(std::string username) = 0;
	virtual float getPlayerAverageAnswerTime(std::string username) = 0;
	virtual int getNumOfCorrectAnswers(std::string username) = 0;
	virtual	int getNumOfTotalAnswers(std::string username) = 0;
	virtual	int getNumOfPlayerGames(std::string username) = 0;
	virtual float getTotalTimeForAnswer(std::string username) = 0;
	virtual	double getSpecificStatistic(std::string username, std::string statisticName) = 0;
	virtual std::list<std::string> getUsernames() = 0;
	virtual bool open() = 0;
	virtual void close() = 0;
};

