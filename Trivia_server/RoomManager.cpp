#include "RoomManager.h"

RoomManager::RoomManager()
{
	this->LatestRoomID = 0;
}

void RoomManager::createRoom(LoggedUser* roomCreator, RoomData* roomData)
{
	Room* newRoom = new Room();
	newRoom->addUser(roomCreator);
	roomData->id = LatestRoomID;
	newRoom->setData(roomData);
	this->m_rooms.push_back(newRoom);
	this->LatestRoomID++;
}

bool RoomManager::deleteRoom(int ID)
{
	for (int i = 0; i < this->m_rooms.size(); i++)
	{
		if (this->m_rooms[i]->getRoomData()->id == ID)
		{
			this->m_rooms.erase(this->m_rooms.begin() + i);
			LatestRoomID--;
			return true;
		}
	}
	return false;
}

GetRoomStateResponse* RoomManager::getRoomState(int ID)
{
	GetRoomStateResponse* getRoomStateResponse = new GetRoomStateResponse();
	std::vector<Room*> rooms = this->getRooms();
	for (int i = 0; i < rooms.size(); i++)
	{
		if (rooms[i]->getRoomData()->id == ID)
		{
			getRoomStateResponse->status = SUCCESS_CODE;
			getRoomStateResponse->hasGameBegun = rooms[i]->getRoomData()->isActive;
			getRoomStateResponse->answerTimeout = rooms[i]->getRoomData()->timePerQuestion;
			getRoomStateResponse->questionCount = rooms[i]->getRoomData()->questionCount;
			for (int j = 0; j < rooms[i]->getAllUsers().size(); j++)
			{
				getRoomStateResponse->players.push_back(rooms[i]->getAllUsers()[j]->getUsername());
			}
		}
		else
		{
			getRoomStateResponse->status = ERROR_CODE;
		}
		return getRoomStateResponse;
	}
}

std::vector<Room*> RoomManager::getRooms()
{
	return this->m_rooms;
}
