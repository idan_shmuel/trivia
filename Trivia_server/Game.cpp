#include "Game.h"

Game::Game(Game &game)
{
	this->m_questions = game.m_questions;
	this->m_players = game.m_players;
}

Game::Game(std::vector<Question> questions, std::map<LoggedUser*, GameData> players, int roomID)
{
	this->m_questions = questions;
	this->m_players = players;
	this->m_roomID = roomID;
}

Question Game::getQuestionForUser(LoggedUser* user)
{
	std::map<LoggedUser*, GameData>::iterator it;
	for (it = this->m_players.begin(); it != this->m_players.end(); ++it)
	{
		if (it->first->getUsername() == user->getUsername())
		{
			it->second.currentQuestion = this->m_questions[it->second.currentQuestionNum];
			it->second.currentQuestionNum++;
			return it->second.currentQuestion;
		}
	}
}

bool Game::isPlayerInGame(LoggedUser* user)
{
	std::map<LoggedUser*, GameData>::iterator it;
	for (it = this->m_players.begin(); it != this->m_players.end(); ++it)
	{
		if (it->first->getUsername() == user->getUsername())
		{
			return true;
		}
	}
	return false;
}

std::pair <bool, int> Game::submitAnswer(std::string answerSelected, LoggedUser* user)
{
	std::map<LoggedUser*, GameData>::iterator it;
	bool submitAnswer;
	int rightAnswerID;
	for (it = this->m_players.begin(); it != this->m_players.end(); ++it)
	{
		if (it->first->getUsername() == user->getUsername())
		{
			if (it->second.currentQuestion.getCorrectAnswer() == answerSelected)
			{
				submitAnswer = true;
			}
			else
			{
				submitAnswer = false;
			}

			//check for the right answer id
			for (int i = 0; i < it->second.currentQuestion.getPossibleAnswers().size(); i++)
			{
				if (it->second.currentQuestion.getPossibleAnswers()[i] == it->second.currentQuestion.getCorrectAnswer())
				{
					rightAnswerID = i + 1;
					break;
				}
			}
		}
	}
	return std::make_pair(submitAnswer, rightAnswerID);
}


void Game::removePlayer(LoggedUser* user)
{
	this->m_players.erase(user);
}

std::map<LoggedUser*, GameData> Game::getPlayers()
{
	return this->m_players;
}

int Game::getRoomID()
{
	return this->m_roomID;
}
