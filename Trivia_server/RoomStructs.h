#pragma once
#include "pch.h"

typedef struct RoomData
{
	RoomData(unsigned int id, unsigned int timePerQuestion, unsigned int isActive, unsigned int maxPlayers, unsigned int questionCount, std::string name)
	{
		this->id = id;
		this->isActive = isActive;
		this->maxPlayers = maxPlayers;
		this->name = name;
		this->questionCount = questionCount;
		this->timePerQuestion = timePerQuestion;
	}
	RoomData()
	{
		this->id = 0;
		this->isActive = 0;
		this->maxPlayers = 0;
		this->name = "";
		this->questionCount = 0;
		this->timePerQuestion = 0;
	}
	unsigned int id;
	unsigned int timePerQuestion;
	unsigned int isActive;
	unsigned int maxPlayers;
	unsigned int questionCount;
	std::string name;
}RoomData;