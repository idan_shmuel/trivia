#include "LoginManager.h"

LoginManager::LoginManager()
{
	this->m_database = new SqliteDataBase();
}

LoginManager::~LoginManager()
{
	delete this->m_database;
}

int LoginManager::signup(std::string username, std::string password, std::string email, std::string address, std::string phoneNumber, std::string birthDate)
{
	if (!this->m_database->doesUserValid(username))
	{
		return WRONG_USERNAME;
	}
	if (this->m_database->doesUserExist(username))
	{
		return USER_ALREADY_EXSITS;
	}
	if (!this->doesPasswordValid(password))
	{
		return PASSWORD_PROBLEM;
	}
	if (!this->doesEmailValid(email))
	{
		return WRONG_EMAIL;
	}
	if (!this->doesAddressValid(address))
	{
		return WRONG_ADDRESS;
	}
	if (!this->doesphonenumberValid(phoneNumber))
	{
		return WRONG_PHONE_NUMBER;
	}
	if (!this->doesBirthDateValid(birthDate))
	{
		return WRONG_BIRTH_DATE;
	}
	this->m_database->addNewUser(username, password, email, address, phoneNumber, birthDate);
	return SUCCESS_CODE;
}

int LoginManager::login(std::string username, std::string password)
{
	LoggedUser* loggedUser = new LoggedUser(username);
	if (!this->m_database->doesUserExist(username))
	{
		return USER_DOES_NOT_EXSITS;
	}
	if (!this->m_database->doesPasswordMatch(username, password))
	{
		return PASSWORD_PROBLEM;
	}
	//user is already connected
	for (int i = 0; i < this->m_loggedUsers.size(); i++)
	{
		if (this->m_loggedUsers[i]->getUsername() == username)
		{
			return USER_ALREADY_CONNECTED;
		}
	}
	//login success
	this->m_loggedUsers.push_back(loggedUser);
	return SUCCESS_CODE;
}

bool LoginManager::logout(std::string username)
{
	for (int i = 0; i < this->m_loggedUsers.size(); i++)
	{
		if (this->m_loggedUsers[i]->getUsername() == username)
		{
			this->m_loggedUsers.erase(this->m_loggedUsers.begin() + i);
			return true;
		}
	}
	return false;
}

bool LoginManager::doesPasswordValid(std::string password)
{
	std::regex validationExpressions[4];
	validationExpressions[0] = ("[a-z]+");
	validationExpressions[1] = ("[A-Z]+");
	validationExpressions[2] = ("[0-9]+");
	validationExpressions[3] = ("[!@#$%^&*]+");

	for (int i = 0; i < 4; i++)
	{
		if (!std::regex_search(password, validationExpressions[i]))
		{
			return false;
		}
	}

	return (password.size() >= MIN_PASSWORD_LENGTH);
}

bool LoginManager::doesEmailValid(std::string email)
{
	std::regex validationExpression("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
	return std::regex_match(email, validationExpression);
}

bool LoginManager::doesAddressValid(std::string address)
{
	std::regex validationExpression("^([a-zA-Z]+), ([0-9]+), ([a-zA-Z]+)$");
	return std::regex_match(address, validationExpression);
}

bool LoginManager::doesphonenumberValid(std::string phoneNumber)
{
	std::regex validationExpression("0[[:digit:]]{1}-[[:digit:]]{7}");
	std::regex validationExpression2("05[[:digit:]]{1}-[[:digit:]]{7}");
	return (std::regex_match(phoneNumber, validationExpression) || std::regex_match(phoneNumber, validationExpression2));
}

bool LoginManager::doesBirthDateValid(std::string birthDate)
{
	std::regex validationExpression("^([0]?[1-9]|[1|2][0-9]|[3][0|1])[.]([0]?[1-9]|[1][0-2])[.]([0-9]{4})$");
	std::regex validationExpression2("^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([0-9]{4})$");
	return (std::regex_match(birthDate, validationExpression) || std::regex_match(birthDate, validationExpression2));
}
