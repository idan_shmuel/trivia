#pragma once
#pragma comment(lib, "Ws2_32.lib")
#include <iostream>
#include <map>
#include <WinSock2.h>
#include <Windows.h>
#include <string>
#include <algorithm>
#include <thread>
#include <utility>
#include <ctime>
#include <cmath>
#include <chrono>
#include <bitset>
#include <io.h>
#include <regex>
#include <time.h>
#include <stdlib.h>
#include "json-develop/single_include/nlohmann/json.hpp"
#include <sstream>
#include <fstream>
#define BYTE 8
#define MIN_PASSWORD_LENGTH 8
#define ERROR_CODE 0
#define LOGIN_CODE 1
#define SUCCESS_CODE 1
#define SIGNUP_CODE 2
#define LOGOUT_CODE 3
#define GET_ROOMS_CODE 4
#define GET_PLAYERS_IN_ROOM_CODE 5
#define JOIN_ROOM_CODE 6
#define CREATE_ROOM_CODE 7
#define HIGH_SCORE_CODE 8
#define CLOSE_ROOM_CODE 9
#define START_GAME_CODE 10
#define GET_ROOM_STATE_CODE 11
#define LEAVE_ROOM_CODE 12
#define GET_GAME_RESULTS_CODE 13
#define SUBMIT_ANSWER_CODE 14
#define GET_QUESTION_CODE 15
#define LEAVE_GAME_CODE 16
#define PLAYER_STATISTICS_CODE 17
#define USER_ALREADY_EXSITS 2 
#define USER_ALREADY_CONNECTED 4
#define USER_DOES_NOT_EXSITS 2
#define PASSWORD_PROBLEM 3 
#define WRONG_EMAIL 4 
#define WRONG_ADDRESS 5
#define WRONG_PHONE_NUMBER 6
#define WRONG_BIRTH_DATE 7
#define WRONG_USERNAME 8

