#pragma once
#include "pch.h"
#include "requestStructs.h"

class JsonRequestPacketDeserializer
{
public:
	JsonRequestPacketDeserializer();

	static LoginRequest deserializeLoginRequest(unsigned char* buffer);
	static SignupRequest deserializeSignupRequest(unsigned char* buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(unsigned char* buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(unsigned char* buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(unsigned char* buffer);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(unsigned char* buffer);
};

