#include "StatisticsManager.h"

StatisticsManager::StatisticsManager()
{
	this->m_database = new SqliteDataBase();
}

StatisticsManager::~StatisticsManager()
{
	delete this->m_database;
}

void StatisticsManager::updateUserAvgAnswerTime(std::string username, float totalTimeForAnswer)
{
	float avgAnswerTime;
	this->m_database->updateTotalTimeForAnswer(username, totalTimeForAnswer);
	avgAnswerTime =(this->m_database->getTotalTimeForAnswer(username) / this->m_database->getNumOfTotalAnswers(username));
	this->m_database->updateUserAvgAnswerTime(username, avgAnswerTime);
}

void StatisticsManager::increaseUserNumCorrectAns(std::string username)
{
	this->m_database->increaseUserNumCorrectAns(username);
}

void StatisticsManager::increaseUserNumOfGames(std::string username)
{
	this->m_database->increaseUserNumOfGames(username);
}

void StatisticsManager::increaseUserNumTotalAnswers(std::string username)
{
	this->m_database->increaseUserNumTotalAnswers(username);	
}

std::map<std::string, double> StatisticsManager::getUserStatistics(std::string username)
{
	std::map<std::string, double> userStatistics;
	userStatistics["playerAverageAnswerTime"] = this->m_database->getPlayerAverageAnswerTime(username);
	userStatistics["numOfCorrectAnswers"] = this->m_database->getNumOfCorrectAnswers(username);
	userStatistics["numOfPlayerGames"] = this->m_database->getNumOfPlayerGames(username);
	userStatistics["numOfTotalAnswers"] = this->m_database->getNumOfTotalAnswers(username);
	return userStatistics;
}

std::vector<std::pair<std::string, double>> StatisticsManager::getTopFivePlayers()
{
	std::vector<std::pair<std::string, double>> users;
	std::vector<std::pair<std::string, double>> topFivePlayers;
	std::list<std::string> usersList = this->m_database->getUsernames();
	for (auto it = usersList.begin(); it != usersList.end(); ++it)
	{
		users.push_back(std::make_pair(*it, getUserScore(*it)));
	}
	// sort the vector by increasing order of its pair's second value
	std::sort(users.begin(), users.end(), sortByVal);
	int limit = users.size();
	if (limit > 5)
	{
		limit = 5;
	}
	for (int i = 0; i < limit; i++)
	{
		topFivePlayers.push_back(users[i]);
	}
	return topFivePlayers;
}

double StatisticsManager::getUserScore(std::string username)
{
	double userScore = 0;
	std::map<std::string, double> userStatistics;
	userStatistics = this->getUserStatistics(username); 
	if (userStatistics["numOfTotalAnswers"] == 0 || userStatistics["playerAverageAnswerTime"] == 0)
	{
		userScore = 0;
	}
	else
	{
		//For the score we will take the ratio between the correct answers and all the answers, divide by the average time of each answer and multiply by 100.
		userScore = round(((userStatistics["numOfCorrectAnswers"] / userStatistics["numOfTotalAnswers"]) / userStatistics["playerAverageAnswerTime"]) * 100);
	}
	return userScore;
}

bool StatisticsManager::sortByVal(const std::pair<std::string, double>& l, const std::pair<std::string, double>& r)
{
	if (l.second != r.second)
		return l.second > r.second;

	return l.first > r.first;
}