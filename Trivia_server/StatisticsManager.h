#pragma once
#include "IDatabase.h"
#include "SqliteDataBase.h"
#include "pch.h"

class StatisticsManager
{
private:
	IDatabase* m_database;

public:
	StatisticsManager();
	~StatisticsManager();
	void updateUserAvgAnswerTime(std::string username, float totalTimeForAnswer);
	void increaseUserNumCorrectAns(std::string username);
	void increaseUserNumOfGames(std::string username);
	void increaseUserNumTotalAnswers(std::string username);
	std::map<std::string, double> getUserStatistics(std::string username);
	std::vector<std::pair<std::string, double>> getTopFivePlayers();
	double getUserScore(std::string username);
	static bool sortByVal(const std::pair<std::string, double>& l, const std::pair<std::string, double>& r);
};

