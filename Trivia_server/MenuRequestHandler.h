#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"

class MenuRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* m_handlerFactory;
	LoggedUser* m_loggedUser;
	RequestResult handleLogoutRequest(RequestInfo requestInfo);
	RequestResult handleGetRoomsRequest(RequestInfo requestInfo);
	RequestResult handleGetPlayersInRoomRequest(RequestInfo requestInfo);
	RequestResult handleJoinRoomRequest(RequestInfo requestInfo);
	RequestResult handleCreateRoomRequest(RequestInfo requestInfo);
	RequestResult handleHighScoreRequest(RequestInfo requestInfo);
	RequestResult handleGetStatistics(RequestInfo requestInfo);

public:
	MenuRequestHandler(LoggedUser* loggedUser, RequestHandlerFactory* handleFactory);
	virtual bool isRequestRelevant(RequestInfo requestInfo);
	virtual RequestResult handleRequest(RequestInfo requestInfo);

};

